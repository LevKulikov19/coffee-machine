﻿
using System.Drawing;

namespace Coffee
{
    partial class Purse
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Purse));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.pictureBoxCoin10 = new System.Windows.Forms.PictureBox();
            this.pictureBoxCoin5 = new System.Windows.Forms.PictureBox();
            this.pictureBoxCoin2 = new System.Windows.Forms.PictureBox();
            this.pictureBoxCoin1 = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCoin10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCoin5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCoin2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCoin1)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.pictureBoxCoin10, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.pictureBoxCoin5, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.pictureBoxCoin2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.pictureBoxCoin1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(112, 521);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // pictureBoxCoin10
            // 
            this.pictureBoxCoin10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.pictureBoxCoin10.BackgroundImage = global::Coffee.Properties.Resources.Coin10;
            this.pictureBoxCoin10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBoxCoin10.Location = new System.Drawing.Point(6, 395);
            this.pictureBoxCoin10.Margin = new System.Windows.Forms.Padding(5);
            this.pictureBoxCoin10.Name = "pictureBoxCoin10";
            this.pictureBoxCoin10.Size = new System.Drawing.Size(100, 121);
            this.pictureBoxCoin10.TabIndex = 3;
            this.pictureBoxCoin10.TabStop = false;
            this.pictureBoxCoin10.Click += new System.EventHandler(this.pictureBoxCoin10_Click);
            // 
            // pictureBoxCoin5
            // 
            this.pictureBoxCoin5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.pictureBoxCoin5.BackgroundImage = global::Coffee.Properties.Resources.Coin5;
            this.pictureBoxCoin5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBoxCoin5.Location = new System.Drawing.Point(6, 265);
            this.pictureBoxCoin5.Margin = new System.Windows.Forms.Padding(5);
            this.pictureBoxCoin5.Name = "pictureBoxCoin5";
            this.pictureBoxCoin5.Size = new System.Drawing.Size(100, 120);
            this.pictureBoxCoin5.TabIndex = 2;
            this.pictureBoxCoin5.TabStop = false;
            this.pictureBoxCoin5.Click += new System.EventHandler(this.pictureBoxCoin5_Click);
            // 
            // pictureBoxCoin2
            // 
            this.pictureBoxCoin2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.pictureBoxCoin2.BackgroundImage = global::Coffee.Properties.Resources.Coin2;
            this.pictureBoxCoin2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBoxCoin2.Location = new System.Drawing.Point(6, 135);
            this.pictureBoxCoin2.Margin = new System.Windows.Forms.Padding(5);
            this.pictureBoxCoin2.Name = "pictureBoxCoin2";
            this.pictureBoxCoin2.Size = new System.Drawing.Size(100, 120);
            this.pictureBoxCoin2.TabIndex = 1;
            this.pictureBoxCoin2.TabStop = false;
            this.pictureBoxCoin2.Click += new System.EventHandler(this.pictureBoxCoin2_Click);
            // 
            // pictureBoxCoin1
            // 
            this.pictureBoxCoin1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.pictureBoxCoin1.BackgroundImage = global::Coffee.Properties.Resources.Coin1;
            this.pictureBoxCoin1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBoxCoin1.ErrorImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxCoin1.ErrorImage")));
            this.pictureBoxCoin1.Location = new System.Drawing.Point(6, 5);
            this.pictureBoxCoin1.Margin = new System.Windows.Forms.Padding(5);
            this.pictureBoxCoin1.Name = "pictureBoxCoin1";
            this.pictureBoxCoin1.Size = new System.Drawing.Size(100, 120);
            this.pictureBoxCoin1.TabIndex = 0;
            this.pictureBoxCoin1.TabStop = false;
            this.pictureBoxCoin1.Click += new System.EventHandler(this.pictureBoxCoin1_Click);
            // 
            // Purse
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(63)))), ((int)(((byte)(27)))));
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "Purse";
            this.Size = new System.Drawing.Size(112, 521);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCoin10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCoin5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCoin2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCoin1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.PictureBox pictureBoxCoin10;
        private System.Windows.Forms.PictureBox pictureBoxCoin5;
        private System.Windows.Forms.PictureBox pictureBoxCoin2;
        private System.Windows.Forms.PictureBox pictureBoxCoin1;
    }
}
