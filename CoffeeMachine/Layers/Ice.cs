﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Coffee.Layers
{

    class Ice : DrinkLayer
    {
        public uint Count
        {
            get;
        }

        public Ice (uint count) : base()
        {
            Count = count;
            Duration = 1 * count;
        }
    }
}
