﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Coffee.Layers
{
    class Fluid : DrinkLayer
    {
        public Color MainColor
        {
            get;
            set;
        }

        public Fluid(double duration, Color color) : base()
        {
            Duration = duration;
            MainColor = color;
        }
    }
}
