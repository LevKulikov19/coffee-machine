using Coffee.Display;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Coffee
{
    public partial class CoffeeMachineDisplay : UserControl
    {
        public delegate void CoffeeMachineDisplayEvent(object sender, Drink drink);
        public event CoffeeMachineDisplayEvent DrinkParametr;
        public event CoffeeMachineDisplayEvent Refund;
        public event CoffeeMachineDisplayEvent Payment;
        public event CoffeeMachineDisplayEvent ReadyToCook;
        public event CoffeeMachineDisplayEvent Cooking;
        
        UserControl active;
        uint balance = 0;
        public uint Balance
        {
            get => balance;
            set
            {
                balance = value;
            }
        }

        public CoffeeMachineDisplay()
        {
            InitializeComponent();
            ViewPageStart();
        }

        public void DrinkPaid(uint balance)
        {
            if (!(active is DisplayPayment)) throw new Exception("Invalid step to drink paid");
            this.balance = balance;
            (active as DisplayPayment).Balance = balance;

        }

        public void ReadyToCooking (bool ready, Drink drink)
        {
            if (ready)
                ViewPageCooking(drink);
            else
                ViewPageDrinkWarning(drink);
        }

        public void CookingStage(uint percentage)
        {
            if (percentage >= 100) return;
            if (!(active is DisplayCooking)) throw new Exception("Invalid step to indicate cooking end");
            (active as DisplayCooking).CompletionPercentage = percentage;

        }

        public void FinishCooking()
        {
            if (!(active is DisplayCooking)) throw new Exception("Incorrect stage to end cooking");
            balance = 0;
            ViewPageFinished();
        }

        private void DisplayStart_Start()
        {
            ViewPageChoiceDrink();
        }

        private void ViewPageStart ()
        {
            DisplayStart displayStart = new DisplayStart();
            displayStart.Start += DisplayStart_Start;
            ViewPage(displayStart);
        }

        private void ViewPageChoiceDrink()
        {
            DisplayChoiceDrink displayChoiceDrink = new DisplayChoiceDrink();
            displayChoiceDrink.ChoiceDrink += DisplayChoiceDrink_ChoiceDrink;
            ViewPage(displayChoiceDrink);
        }

        private void ViewPageDrinkParameters(Drink drink)
        {
            DisplayDrinkParameters displayDrinkParameters = new DisplayDrinkParameters(drink);
            displayDrinkParameters.Back += DisplayDrinkParameters_Back;
            displayDrinkParameters.Payment += DisplayDrinkParameters_Payment;
            DrinkParametr(this, drink);
            ViewPage(displayDrinkParameters);
        }

        private void DisplayDrinkParameters_Payment(object sender, Drink drink)
        {
            ViewPagePayment(drink, balance);
        }

        private void DisplayDrinkParameters_Back(object sender, Drink drink)
        {
            ViewPageChoiceDrink();
        }

        private void DisplayChoiceDrink_ChoiceDrink(object sender, Drink drink)
        {
            ViewPageDrinkParameters(drink);
        }

        private void ViewPagePayment(Drink drink, uint balance)
        {
            DisplayPayment displayPayment = new DisplayPayment(drink);
            displayPayment.Balance = balance;
            displayPayment.Back += DisplayPayment_Back;
            displayPayment.Cooking += DisplayPayment_Cooking;
            ViewPage(displayPayment);
            Payment(this, drink);
        }

        private void ViewPageDrinkWarning(Drink drink)
        {
            DisplayDrinkWarning displayDrinkWarning = new DisplayDrinkWarning(drink);
            displayDrinkWarning.Continue += DisplayDrinkWarning_Continue;
            ViewPage(displayDrinkWarning);
        }

        private void DisplayDrinkWarning_Continue(object sender, Drink drink)
        {
            ViewPageCooking(drink);
        }

        private void DisplayPayment_Cooking(object sender, Drink drink)
        {
            ReadyToCook(this, drink);
        }

        private void DisplayPayment_Back(object sender, Drink drink)
        {
            balance = 0;
            Refund(this, drink);
            ViewPageDrinkParameters(drink);
        }

        private void ViewPageCooking(Drink drink)
        {
            DisplayCooking displayCooking = new DisplayCooking(drink);
            ViewPage(displayCooking);
            Cooking(this, drink);
        }

        private void ViewPageFinished()
        {
            DisplayFinished displayFinished = new DisplayFinished();
            displayFinished.Start += DisplayFinished_Start;
            ViewPage(displayFinished);
        }

        private void DisplayFinished_Start(object sender)
        {
            ViewPageStart();
        }

        private void ViewPage (UserControl page)
        {
            if (active != null) active.Dispose();
            this.Controls.Add(page);
            active = page;
            timerToStart.Start();
            if (active is DisplayStart) timerToStart.Stop();
            else if (active is DisplayCooking) timerToStart.Stop();
            else
            {
                timerToStart.Stop();
                timerToStart.Start();
            }
        }

        private void timerToStart_Tick(object sender, EventArgs e)
        {
            ViewPageStart();
        }

    }
}
