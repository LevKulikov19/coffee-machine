﻿
using System.Drawing;

namespace Coffee
{
    partial class CoffeeMachineDisplay
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timerToStart = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // timerToStart
            // 
            this.timerToStart.Interval = 100000;
            this.timerToStart.Tick += new System.EventHandler(this.timerToStart_Tick);
            // 
            // CoffeeMachineDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(187)))), ((int)(((byte)(231)))));
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "CoffeeMachineDisplay";
            this.Size = new System.Drawing.Size(85, 135);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timerToStart;
    }
}
