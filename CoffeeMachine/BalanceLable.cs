﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Coffee
{
    public partial class BalanceLable : UserControl
    {
        uint balance;
        public uint Balance
        {
            get => balance;
            set
            {
                balance = value;
                RefreshBalance();
            }
        }

        public BalanceLable()
        {
            InitializeComponent();
            Balance = 0;
        }

        void RefreshBalance()
        {
            label1.Text = Balance.ToString();
        }
    }
}
