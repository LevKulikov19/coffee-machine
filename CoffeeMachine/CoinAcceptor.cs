﻿using Coffee.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Text;
using System.Windows.Forms;

namespace Coffee
{
    public partial class CoinAcceptor : UserControl
    {
        public delegate void CoinAcceptorEvent (object sender, uint cash);
        public event CoinAcceptorEvent Taken;
        uint cash = 0;

        List<int> coins;

        public CoinAcceptor()
        {
            InitializeComponent();
            cash = 0;
            coins = new List<int>();
            RefrashCoins();
            Refresh();
        }

        public void addSurrender (uint cash)
        {
            this.cash += cash;
            RefrashCoins();
            Refresh();
        }

        private void CoinAcceptor_Click(object sender, EventArgs e)
        {
            if (cash != 0 && Taken != null)
            {
                Taken(this, cash);
                cash = 0;
                RefrashCoins();
                Refresh();
            }
        }

        private void CoinAcceptor_Paint(object sender, PaintEventArgs e)
        {
            RefreshCoinAcceptor();
        }

        private void RefreshCoinAcceptor()
        {
            Graphics graphicsPaintSpace = this.CreateGraphics();
            BufferedGraphicsContext bufferedGraphicsContext = BufferedGraphicsManager.Current;
            BufferedGraphics bufferedGraphics = bufferedGraphicsContext.Allocate(graphicsPaintSpace, this.DisplayRectangle);
            Graphics gPaint = bufferedGraphics.Graphics;

            gPaint.Clear(BackColor);
            gPaint.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CoinAcceptor));
            Bitmap image = (Bitmap)(resources.GetObject("$this.BackgroundImage"));
            int widthBack = ClientRectangle.Width;
            int heightBack = ClientRectangle.Height;
            ImageAttributes wrapMode = new ImageAttributes();
            wrapMode.SetWrapMode(WrapMode.Clamp);
            gPaint.DrawImage(image, new Rectangle(0, 0, widthBack, heightBack), 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);

            int countCoin10 = 0;
            int countCoin5 = 0;
            int countCoin2 = 0;
            int countCoin1 = 0;

            int widthCoin = 25;
            int heightCoin = 16;
            int floorCoin = 38;
            int marginCoin = 4;
            for (int i = 0; i < coins.Count; i++)
            {
                Bitmap imageCoin = null;
                Point[] destPointsCoin = new Point[3];

                if (coins[i] == 1 && countCoin1 < 10)
                {
                    imageCoin = global::Coffee.Properties.Resources.Coin1;
                    destPointsCoin[0] = new Point(marginCoin + widthCoin * 3, floorCoin - heightCoin - countCoin1 * 3);
                    destPointsCoin[1] = new Point(marginCoin + widthCoin * 3 + widthCoin, floorCoin - heightCoin - countCoin1 * 3);
                    destPointsCoin[2] = new Point(widthCoin * 3, floorCoin - countCoin1 * 3);
                    countCoin1++;
                }
                if (coins[i] == 2 && countCoin2 < 10)
                {
                    imageCoin = global::Coffee.Properties.Resources.Coin2;
                    destPointsCoin[0] = new Point(marginCoin + widthCoin*2, floorCoin - heightCoin - countCoin2 * 3);
                    destPointsCoin[1] = new Point(marginCoin + widthCoin*2 + widthCoin, floorCoin - heightCoin - countCoin2 * 3);
                    destPointsCoin[2] = new Point(widthCoin*2, floorCoin - countCoin2 * 3);
                    countCoin2++;
                }
                if (coins[i] == 5 && countCoin5 < 10)
                {
                    imageCoin = global::Coffee.Properties.Resources.Coin5;
                    destPointsCoin[0] = new Point(marginCoin + widthCoin, floorCoin - heightCoin - countCoin5 * 3);
                    destPointsCoin[1] = new Point(marginCoin + widthCoin + widthCoin, floorCoin - heightCoin - countCoin5 * 3);
                    destPointsCoin[2] = new Point(widthCoin, floorCoin - countCoin5 * 3);
                    countCoin5++;
                }
                if (coins[i] == 10 && countCoin10 < 10)
                {
                    imageCoin = global::Coffee.Properties.Resources.Coin10;
                    destPointsCoin[0] = new Point(marginCoin, floorCoin - heightCoin - countCoin10 * 3);
                    destPointsCoin[1] = new Point(widthCoin + marginCoin, floorCoin - heightCoin - countCoin10 * 3);
                    destPointsCoin[2] = new Point(2, floorCoin - countCoin10 * 3);
                    countCoin10++;
                }

                if (imageCoin != null)
                {
                    Rectangle imageView = new Rectangle(0, 0, imageCoin.Width, imageCoin.Height);
                    gPaint.DrawImage(imageCoin, destPointsCoin, imageView, GraphicsUnit.Pixel, wrapMode);
                }
                if (imageCoin != null) imageCoin.Dispose();
            }


            bufferedGraphics.Render(graphicsPaintSpace);
            gPaint.Dispose();

        }

        private void RefrashCoins ()
        {
            coins.Clear();
            int cashTemp = (int)cash;

            while (cashTemp != 0)
            {
                if (cashTemp >= 10)
                {
                    cashTemp -= 10;
                    coins.Add(10);
                }
                else if (cashTemp >= 5)
                {
                    cashTemp -= 5;
                    coins.Add(5);
                }
                else if (cashTemp >= 2)
                {
                    cashTemp -= 2;
                    coins.Add(2);
                }
                else
                {
                    cashTemp -= 1;
                    coins.Add(1);
                }
            }
        }
    }
}
