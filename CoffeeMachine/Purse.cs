﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;
using System.Windows.Forms;

namespace Coffee
{
    public partial class Purse : UserControl
    {
        public delegate void PurseEvent(object sender, int coin);
        public event PurseEvent Pay;

        private uint balance;
        public uint Balance
        {
            get => balance;
            set
            {
                balance = value;
                RefreshState();
            }
        }

        public Purse()
        {
            InitializeComponent();
            RefreshState();
        }

        private void pictureBoxCoin1_Click(object sender, EventArgs e)
        {
            Pay(this, 1);
        }

        private void pictureBoxCoin2_Click(object sender, EventArgs e)
        {
            Pay(this, 2);
        }

        private void pictureBoxCoin5_Click(object sender, EventArgs e)
        {
            Pay(this, 5);
        }

        private void pictureBoxCoin10_Click(object sender, EventArgs e)
        {
            Pay(this, 10);
        }
        
        private void RefreshState()
        {
            Image imageCoin1 = global::Coffee.Properties.Resources.Coin1;
            Image imageCoin2 = global::Coffee.Properties.Resources.Coin2;
            Image imageCoin5 = global::Coffee.Properties.Resources.Coin5;
            Image imageCoin10 = global::Coffee.Properties.Resources.Coin10;

            if (Balance < 10)
            {
                imageCoin10 = DrawAsGrayscale(imageCoin10);
                if (Balance < 5)
                {
                    imageCoin5 = DrawAsGrayscale(imageCoin5);
                    if (Balance < 2)
                    {
                        imageCoin2 = DrawAsGrayscale(imageCoin2);
                        if (Balance < 1)
                        {
                            imageCoin1 = DrawAsGrayscale(imageCoin1);
                        }
                    }
                }
            }

            this.pictureBoxCoin1.BackgroundImage = imageCoin1;
            this.pictureBoxCoin2.BackgroundImage = imageCoin2;
            this.pictureBoxCoin5.BackgroundImage = imageCoin5;
            this.pictureBoxCoin10.BackgroundImage = imageCoin10;
        }
        
        private Bitmap GetArgbCopy(Image sourceImage)
        {
            Bitmap bmpNew = new Bitmap(sourceImage.Width, sourceImage.Height, PixelFormat.Format32bppArgb);

            using (Graphics graphics = Graphics.FromImage(bmpNew))
            {
                graphics.DrawImage(sourceImage, new Rectangle(0, 0, bmpNew.Width, bmpNew.Height), new Rectangle(0, 0, bmpNew.Width, bmpNew.Height), GraphicsUnit.Pixel);
                graphics.Flush();
            }


            return bmpNew;
        }
        
        private Bitmap ApplyColorMatrix(Image sourceImage, ColorMatrix colorMatrix)
        {
            Bitmap bmp32BppSource = GetArgbCopy(sourceImage);
            Bitmap bmp32BppDest = new Bitmap(bmp32BppSource.Width, bmp32BppSource.Height, PixelFormat.Format32bppArgb);

            using (Graphics graphics = Graphics.FromImage(bmp32BppDest))
            {
                ImageAttributes bmpAttributes = new ImageAttributes();
                bmpAttributes.SetColorMatrix(colorMatrix);

                graphics.DrawImage(bmp32BppSource, new Rectangle(0, 0, bmp32BppSource.Width, bmp32BppSource.Height),
                                    0, 0, bmp32BppSource.Width, bmp32BppSource.Height, GraphicsUnit.Pixel, bmpAttributes);
            }

            bmp32BppSource.Dispose();
            return bmp32BppDest;
        }

        private Bitmap DrawAsGrayscale(Image sourceImage)
        {
            ColorMatrix colorMatrix = new ColorMatrix(new float[][]
                                {
                            new float[]{.3f, .3f, .3f, 0, 0},
                            new float[]{.59f, .59f, .59f, 0, 0},
                            new float[]{.11f, .11f, .11f, 0, 0},
                            new float[]{0, 0, 0, 1, 0},
                            new float[]{0, 0, 0, 0, 1}
                                });


            return ApplyColorMatrix(sourceImage, colorMatrix);
        }
    }
}
