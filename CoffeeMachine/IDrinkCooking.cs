﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Coffee
{
    public interface IDrinkCooking
    {
        public double getDurationCooking();
        public List<DrinkLayer> getDrinkLayers();
    }
}
