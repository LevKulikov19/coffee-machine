﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Coffee
{
    interface IDisplayDrinkParametrs
    {
        public string getName();
        public List<string> getNameParametrs();
        public List<string> getNameValuesParametr(string name);
        public void setValueParametr(string nameParam, string nameValue);
        public List<string> getNameCurrentValuesParametrs();
        public string getNameCurrentValueParametr(string nameParam);
    }
}
