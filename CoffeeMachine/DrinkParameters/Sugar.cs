﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Coffee.DrinkParameters
{
    class Sugar : DrinkParameter
    {
        public enum SugarEnum
        {
            WithoutSugar = 0,
            WithSugar = 1,
        }

        public SugarEnum Exists
        {
            get
            {
                return (SugarEnum)state;
            }
        }

        public Sugar(SugarEnum state, bool changeable = true) : base("Сахар")
        {
            this.state = (int)state;
            isChangeable = changeable;

            titleTable.Add(0, "Без сахара");
            titleTable.Add(1, "С сахаром");
        }

        public bool setState(SugarEnum state)
        {
            if (isChangeable)
            {
                this.state = (int)state;
                ParameterChange(this);
                return true;
            }
            return false;
        }
    }
}
