﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Coffee.DrinkParameters
{
    class TypeCoffee : DrinkParameter
    {
        public enum Type
        {
            Instant = 0,
            Granulated = 1
        }

        public Type State
        {
            get => (Type)this.state;
        }

        public TypeCoffee(Type state, bool changeable = true) : base("Тип кофе")
        {
            this.state = (int)state;
            isChangeable = changeable;

            titleTable.Add((int)Type.Instant, "Растворимый");
            titleTable.Add((int)Type.Granulated, "Гранулированный");
        }

        public bool setState(Type state)
        {
            if (isChangeable)
            {
                this.state = (int)state;
                ParameterChange(this);
                return true;
            }
            return false;
        }
    }
}
