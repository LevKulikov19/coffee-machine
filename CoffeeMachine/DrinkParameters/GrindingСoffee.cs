﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Coffee.DrinkParameters
{
    class GrindingСoffee : DrinkParameter
    {
        public enum Grinding
        {
            Small = 0,
            Medium = 1,
            Large = 2
        }


        public Grinding State {
            get => (Grinding)this.state;
        }

        public GrindingСoffee(Grinding state, bool changeable = true) : base("Помол кофе")
        {
            this.state = (int)state;
            isChangeable = changeable;

            titleTable.Add((int)Grinding.Small, "Мелкий помол");
            titleTable.Add((int)Grinding.Medium, "Средний помол");
            titleTable.Add((int)Grinding.Large, "Крупный помол");
        }

        public bool setState(Grinding state)
        {
            if (isChangeable)
            {
                this.state = (int)state;
                ParameterChange(this);
                return true;
            }
            return false;
        }
    }
}
