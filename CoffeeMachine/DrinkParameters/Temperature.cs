﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Coffee.DrinkParameters
{
    class Temperature : DrinkParameter
    {
        public enum TemperatureEnum
        {
            Hot = 0,
            Warm = 1,
            Cold = 2
        }

        public TemperatureEnum State
        {
            get => (TemperatureEnum)this.state;
        }

        public Temperature(TemperatureEnum state, bool changeable = true) : base("Температура")
        {
            this.state = (int)state;
            isChangeable = changeable;

            titleTable.Add((int)TemperatureEnum.Hot, "Горячий");
            titleTable.Add((int)TemperatureEnum.Warm, "Тёплый");
            titleTable.Add((int)TemperatureEnum.Cold, "Холодный");
        }

        public bool setState(TemperatureEnum state)
        {
            if (isChangeable)
            {
                this.state = (int)state;
                ParameterChange(this);
                return true;
            }
            return false;
        }
    }
}
