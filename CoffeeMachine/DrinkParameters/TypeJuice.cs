﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Coffee.DrinkParameters
{
    class TypeJuice : DrinkParameter
    {
        public enum Type
        {
            Apple = 0,
            Orange = 1,
            Tomato = 2,
            Grape = 3
        }

        public Type State
        {
            get => (Type)this.state;
        }

        public TypeJuice(Type state, bool changeable = true) : base("Тип сока")
        {
            this.state = (int)state;
            isChangeable = changeable;

            titleTable.Add((int)Type.Apple, "Яблочный");
            titleTable.Add((int)Type.Orange, "Апельсиновый");
            titleTable.Add((int)Type.Tomato, "Томатный");
            titleTable.Add((int)Type.Grape, "Виноградный");
        }

        public bool setState(Type state)
        {
            if (isChangeable)
            {
                this.state = (int)state;
                ParameterChange(this);
                return true;
            }
            return false;
        }
    }
}
