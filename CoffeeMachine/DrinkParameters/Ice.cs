﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Coffee.DrinkParameters
{
    class Ice : DrinkParameter
    {
        public enum IceEnum
        {
            WithoutIce = 0,
            WithIce = 1,
        }

        public IceEnum Exists
        {
            get
            {
                return (IceEnum)state;
            }
        }

        public Ice(IceEnum state, bool changeable = true) : base("Лёд")
        {
            this.state = (int)state;
            isChangeable = changeable;

            titleTable.Add(0, "Без льда");
            titleTable.Add(1, "Со льдом");
        }

        public bool setState(IceEnum state)
        {
            if (isChangeable)
            {
                this.state = (int)state;
                ParameterChange(this);
                return true;
            }
            return false;
        }
    }
}
