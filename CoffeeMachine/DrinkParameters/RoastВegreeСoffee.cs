﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Coffee.DrinkParameters
{
    class RoastВegreeСoffee : DrinkParameter
    {
        public enum RoastВegree
        {
            Weak = 0,
            Medium = 1,
            Strong = 2
        }

        public RoastВegree State
        {
            get => (RoastВegree)this.state;
        }

        public RoastВegreeСoffee(RoastВegree state, bool changeable = true) : base("Степень обжарки кофе")
        {
            this.state = (int)state;
            isChangeable = changeable;

            titleTable.Add((int)RoastВegree.Weak, "Слабая обжарка");
            titleTable.Add((int)RoastВegree.Medium, "Средняя обжарка");
            titleTable.Add((int)RoastВegree.Strong, "Сильна обжарка");
        }

        public bool setState(RoastВegree state)
        {
            if (isChangeable)
            {
                this.state = (int)state;
                ParameterChange(this);
                return true;
            }
            return false;
        }
    }
}
