﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Coffee.DrinkParameters
{
    class Volume : DrinkParameter
    {
        public enum VolumeEnum
        {
            Small = 0,
            Medium = 1,
            Large = 2
        }

        public VolumeEnum State
        {
            get => (VolumeEnum)this.state;
        }

        public Volume(VolumeEnum state, bool changeable = true) : base("Объём")
        {
            this.state = (int)state;
            isChangeable = changeable;

            titleTable.Add((int)VolumeEnum.Small, "Маленький");
            titleTable.Add((int)VolumeEnum.Medium, "Средний");
            titleTable.Add((int)VolumeEnum.Large, "Большой");
        }

        public bool setState(VolumeEnum state)
        {
            if (isChangeable)
            {
                this.state = (int)state;
                ParameterChange(this);
                return true;
            }
            return false;
        }
    }
}
