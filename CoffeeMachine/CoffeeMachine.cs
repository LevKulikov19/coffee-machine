using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Coffee
{
    public partial class CoffeeMachine : UserControl
    {
        public delegate void CoffeeMachineEvent(object sender, uint coin);
        public event CoffeeMachineEvent Surrender;
        uint balance = 0;
        Drink drink = null;
        bool isPayment = false;
        uint cooking = 0;
        public CoffeeMachine()
        {
            InitializeComponent();
            display.DrinkParametr += Display_DrinkParametr;
            display.Refund += Display_Refund;
            display.Payment += Display_Payment;
            display.ReadyToCook += Display_ReadyToCook;
            display.Cooking += Display_Cooking;
            coinAcceptor.Taken += CoinAcceptor_Taken;
            cookingArea.AnimationMoveMiddle += CookingArea_AnimationMoveMiddle;
            cookingArea.AnimationMoveEnd += CookingArea_AnimationMoveEnd;
            dispensingArea.BackColor = ColorTranslator.FromHtml("#AA582A");
        }

        private void Display_Refund(object sender, Drink drink)
        {
            coinAcceptor.addSurrender(balance);
            balance = 0;
        }

        private void Display_ReadyToCook(object sender, Drink drink)
        {
            isPayment = false;
            this.drink = drink;
            if (!dispensingArea.Cup)
            {
                dispensingArea.Take -= DispensingArea_Take;
                display.ReadyToCooking(true, drink);
            }
            else
            {
                display.ReadyToCooking(false, drink);
                dispensingArea.Take += DispensingArea_Take;
            }
        }

        private void DispensingArea_Take(bool cup)
        {
            dispensingArea.Take -= DispensingArea_Take;
            display.ReadyToCooking(true, drink);
        }

        private void Display_DrinkParametr(object sender, Drink drink)
        {
            isPayment = false;
        }

        private void Display_Payment(object sender, Drink drink)
        {
            isPayment = true;
            this.drink = drink;
        }

        private void Display_Cooking(object sender, Drink drink)
        {
            dispensingArea.Take -= DispensingArea_Take;
            cookingArea.Cooking(drink);
            isPayment = false;
            if (drink.Price < balance) coinAcceptor.addSurrender(balance - drink.Price);
            balance = 0;
        }

        private void CoinAcceptor_Taken(object sender, uint cash)
        {
            Surrender(this, cash);
        }

        private void CookingArea_AnimationMoveEnd(IDrinkCooking drink)
        {
            dispensingArea.PullOutCup();
        }

        private void CookingArea_AnimationMoveMiddle(IDrinkCooking drink)
        {
            timerCooking.Enabled = true;
            timerCooking.Interval = (int)(drink.getDurationCooking() * 10);
        }

        private void timerCooking_Tick(object sender, EventArgs e)
        {
            cooking++;
            display.CookingStage(cooking);
            if (cooking == 100)
            {
                timerCooking.Enabled = false;
                cooking = 0;
                display.FinishCooking();
            }
        }

        public void Pay (uint cash)
        {
            if (isPayment)
            {
                balance = balance + cash;
                display.DrinkPaid(balance);
            }
            else coinAcceptor.addSurrender(cash);
        }

        private void coinAcceptor_Load(object sender, EventArgs e)
        {

        }

        private void dispensingArea_Paint(object sender, PaintEventArgs e)
        {

        }

        private void CoffeeMachine_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
