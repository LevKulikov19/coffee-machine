﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Coffee
{
    public partial class FormMain : Form
    {
        uint balance;
        uint coin
        {
            get => balance;
            set
            {
                balance = value;
            }
        }
        CoffeeMachine machine;
        Purse purse;
        BalanceLable balanceLable;

        public FormMain()
        {
            machine = new CoffeeMachine();
            purse = new Purse();
            balanceLable = new BalanceLable();
            coin = 100;
            InitializeComponent();

            purse.Pay += Purse_Pay;
            machine.Surrender += Machine_Surrender;

            this.Controls.Add(this.machine);
            this.Controls.Add(this.purse);
            this.Controls.Add(this.balanceLable);
            RefreshBalanceLable();
        }

        private void Machine_Surrender(object sender, uint coin)
        {
            this.coin = this.coin + coin;
            RefreshBalanceLable();
        }

        private void Purse_Pay(object sender, int coin)
        {
            int newBalance = (int)this.coin - coin;
            if (newBalance < 0) return;
            this.coin = this.coin - (uint)coin;
            machine.Pay((uint)coin);
            RefreshBalanceLable();
        }

        private void FormMain_Paint(object sender, PaintEventArgs e)
        {
            RefreshFormMain();
        }

        private void RefreshFormMain()
        {
            Graphics graphics = this.CreateGraphics();

            //background
            Color Backcolor = ColorTranslator.FromHtml("#DDA278");
            graphics.Clear(Backcolor);
            SolidBrush brush = new SolidBrush(ColorTranslator.FromHtml("#55392A"));
            int widht = this.ClientRectangle.Width;
            int height = (int)Math.Round(this.ClientRectangle.Height * 0.2, 0);
            graphics.FillRectangle(brush, 0, this.ClientRectangle.Height - height, widht, height);

            //coffe machine
            machine.Location = new Point((this.ClientRectangle.Width / 2) - (machine.Size.Width / 2),
                                          this.ClientRectangle.Height - height - machine.Size.Height);
            machine.BackColor = Backcolor;

            //purse
            purse.Location = new Point((this.ClientRectangle.Width - 20) - (purse.Size.Width),
                                          this.ClientRectangle.Height - height - purse.Size.Height);

            //balance lable
            balanceLable.Location = new Point((this.ClientRectangle.Width) - (balanceLable.Size.Width), 20);
            

        }

        private void RefreshBalanceLable()
        {
            balanceLable.Balance = coin;
            purse.Balance = coin;
        }

        private void timerAddCoin_Tick(object sender, EventArgs e)
        {
            coin += 100;
            RefreshBalanceLable();
        }
    }
}
