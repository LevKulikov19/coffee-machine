﻿
namespace Coffee
{
    partial class CoffeeMachine
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CoffeeMachine));
            this.display = new Coffee.CoffeeMachineDisplay();
            this.timerCooking = new System.Windows.Forms.Timer(this.components);
            this.coinAcceptor = new Coffee.CoinAcceptor();
            this.cookingArea = new Coffee.CookingArea();
            this.dispensingArea = new Coffee.DispensingArea();
            this.SuspendLayout();
            // 
            // display
            // 
            this.display.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(187)))), ((int)(((byte)(231)))));
            this.display.Balance = ((uint)(0u));
            this.display.Location = new System.Drawing.Point(260, 61);
            this.display.Margin = new System.Windows.Forms.Padding(1);
            this.display.Name = "display";
            this.display.Size = new System.Drawing.Size(130, 187);
            this.display.TabIndex = 0;
            // 
            // timerCooking
            // 
            this.timerCooking.Tick += new System.EventHandler(this.timerCooking_Tick);
            // 
            // coinAcceptor
            // 
            this.coinAcceptor.BackColor = System.Drawing.Color.Transparent;
            this.coinAcceptor.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("coinAcceptor.BackgroundImage")));
            this.coinAcceptor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.coinAcceptor.Location = new System.Drawing.Point(260, 291);
            this.coinAcceptor.Margin = new System.Windows.Forms.Padding(1);
            this.coinAcceptor.Name = "coinAcceptor";
            this.coinAcceptor.Size = new System.Drawing.Size(130, 39);
            this.coinAcceptor.TabIndex = 1;
            this.coinAcceptor.Load += new System.EventHandler(this.coinAcceptor_Load);
            // 
            // cookingArea
            // 
            this.cookingArea.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(33)))), ((int)(((byte)(8)))));
            this.cookingArea.Location = new System.Drawing.Point(260, 366);
            this.cookingArea.Margin = new System.Windows.Forms.Padding(1);
            this.cookingArea.Name = "cookingArea";
            this.cookingArea.Size = new System.Drawing.Size(130, 76);
            this.cookingArea.TabIndex = 2;
            // 
            // dispensingArea
            // 
            this.dispensingArea.BackColor = System.Drawing.Color.Transparent;
            this.dispensingArea.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.dispensingArea.Cup = false;
            this.dispensingArea.Location = new System.Drawing.Point(112, 366);
            this.dispensingArea.Margin = new System.Windows.Forms.Padding(1);
            this.dispensingArea.Name = "dispensingArea";
            this.dispensingArea.Size = new System.Drawing.Size(118, 76);
            this.dispensingArea.TabIndex = 3;
            this.dispensingArea.Paint += new System.Windows.Forms.PaintEventHandler(this.dispensingArea_Paint);
            // 
            // CoffeeMachine
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Controls.Add(this.dispensingArea);
            this.Controls.Add(this.cookingArea);
            this.Controls.Add(this.coinAcceptor);
            this.Controls.Add(this.display);
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "CoffeeMachine";
            this.Size = new System.Drawing.Size(466, 529);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.CoffeeMachine_Paint);
            this.ResumeLayout(false);

        }

        #endregion

        private CoffeeMachineDisplay display;
        private System.Windows.Forms.Timer timerCooking;
        private CoinAcceptor coinAcceptor;
        private CookingArea cookingArea;
        private DispensingArea dispensingArea;
    }
}
