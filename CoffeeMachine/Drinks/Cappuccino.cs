﻿using Coffee.DrinkParameters;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Coffee.Drinks
{
    class Cappuccino : Drink
    {
        Volume volume;
        Sugar sugar;

        const uint basePrice = 60;
        const string name = "Капучино";

        public Cappuccino() : base(name, basePrice)
        {
            volume = new Volume(Volume.VolumeEnum.Small);
            volume.PriceBody = false;
            volume.PriceFactor.Add((int)Volume.VolumeEnum.Small, 1);
            volume.PriceFactor.Add((int)Volume.VolumeEnum.Medium, 1.2);
            volume.PriceFactor.Add((int)Volume.VolumeEnum.Large, 1.4);
            parameters.Add(volume);

            sugar = new Sugar(Sugar.SugarEnum.WithoutSugar);
            sugar.PriceBody = true;
            sugar.PriceFactor.Add((int)Sugar.SugarEnum.WithoutSugar, 0);
            sugar.PriceFactor.Add((int)Sugar.SugarEnum.WithSugar, 5);
            sugar.Change += Sugar_Change;
            parameters.Add(sugar);

            Layers.Add(new Layers.Fluid(5, ColorTranslator.FromHtml("#712822")));
            Layers.Add(new Layers.Fluid(1, ColorTranslator.FromHtml("#FFFFFF")));
            Layers.Add(new Layers.Fluid(1, ColorTranslator.FromHtml("#FEFEFE")));
        }

        private void Sugar_Change(object sender)
        {
            Layers.Clear();
            Layers.Add(new Layers.Fluid(5, ColorTranslator.FromHtml("#712822")));
            Layers.Add(new Layers.Fluid(1, ColorTranslator.FromHtml("#FFFFFF")));
            Layers.Add(new Layers.Fluid(1, ColorTranslator.FromHtml("#FEFEFE")));
            if (sugar.Exists == Sugar.SugarEnum.WithSugar) Layers.Add(new Layers.Fluid(1, ColorTranslator.FromHtml("#FFFFFF")));
        }
    }
}
