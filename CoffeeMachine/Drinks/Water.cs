﻿using Coffee.DrinkParameters;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Coffee.Drinks
{
    class Water : Drink
    {
        Temperature temperature;
        Ice ice;
        Volume volume;

        const uint basePrice = 5;
        const string name = "Вода";

        public Water() : base(name, basePrice)
        {
            temperature = new Temperature(Temperature.TemperatureEnum.Warm);
            temperature.PriceBody = true;
            temperature.PriceFactor.Add((int)Temperature.TemperatureEnum.Cold, 0);
            temperature.PriceFactor.Add((int)Temperature.TemperatureEnum.Warm, 0);
            temperature.PriceFactor.Add((int)Temperature.TemperatureEnum.Hot, 0);
            parameters.Add(temperature);

            ice = new Ice(Ice.IceEnum.WithIce);
            ice.PriceBody = true;
            ice.PriceFactor.Add((int)Ice.IceEnum.WithoutIce, 0);
            ice.PriceFactor.Add((int)Ice.IceEnum.WithIce, 0.5);
            ice.Change += Update_Layers_Change;
            parameters.Add(ice);

            volume = new Volume(Volume.VolumeEnum.Small);
            volume.PriceBody = false;
            volume.PriceFactor.Add((int)Volume.VolumeEnum.Small, 1);
            volume.PriceFactor.Add((int)Volume.VolumeEnum.Medium, 1.2);
            volume.PriceFactor.Add((int)Volume.VolumeEnum.Large, 1.4);
            parameters.Add(volume);

            Layers.Add(new Layers.Fluid(4.5, ColorTranslator.FromHtml("#90BFD4")));
        }

        private void Update_Layers_Change(object sender)
        {
            Layers.Clear();
            Layers.Add(new Layers.Fluid(4.5, ColorTranslator.FromHtml("#90BFD4")));
            if (ice.Exists == Ice.IceEnum.WithIce) Layers.Add(new Layers.Ice(3));
            ParameterChange(this);
        }
    }
}
