﻿using Coffee.DrinkParameters;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Coffee.Drinks
{
    class HotСhocolate : Drink
    {
        Volume volume;

        const uint basePrice = 50;
        const string name = "Горячий шоколад";

        public HotСhocolate() : base(name, basePrice)
        {
            volume = new Volume(Volume.VolumeEnum.Small);
            volume.PriceBody = false;
            volume.PriceFactor.Add((int)Volume.VolumeEnum.Small, 1);
            volume.PriceFactor.Add((int)Volume.VolumeEnum.Medium, 1.2);
            volume.PriceFactor.Add((int)Volume.VolumeEnum.Large, 1.4);
            parameters.Add(volume); 
            Layers.Add(new Layers.Fluid(5, ColorTranslator.FromHtml("#623319")));
        }
    }
}
