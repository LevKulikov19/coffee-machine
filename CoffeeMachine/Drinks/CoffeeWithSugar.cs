﻿using Coffee.DrinkParameters;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Coffee.Drinks
{
    class CoffeeWithSugar : Drink
    {
        GrindingСoffee grinding;
        RoastВegreeСoffee roastВegree;
        TypeCoffee type;

        const uint basePrice = 55;
        const string name = "Кофе с сахаром";

        public CoffeeWithSugar() : base(name, basePrice)
        {
            grinding = new GrindingСoffee(GrindingСoffee.Grinding.Small);
            grinding.PriceBody = true;
            grinding.PriceFactor.Add((int)GrindingСoffee.Grinding.Small, 0);
            grinding.PriceFactor.Add((int)GrindingСoffee.Grinding.Medium, 0);
            grinding.PriceFactor.Add((int)GrindingСoffee.Grinding.Large, 0);
            parameters.Add(grinding);

            roastВegree = new RoastВegreeСoffee(RoastВegreeСoffee.RoastВegree.Weak);
            roastВegree.PriceBody = true;
            roastВegree.PriceFactor.Add((int)RoastВegreeСoffee.RoastВegree.Weak, 0);
            roastВegree.PriceFactor.Add((int)RoastВegreeСoffee.RoastВegree.Medium, 0);
            roastВegree.PriceFactor.Add((int)RoastВegreeСoffee.RoastВegree.Strong, 0);
            parameters.Add(roastВegree);

            type = new TypeCoffee(TypeCoffee.Type.Instant);
            type.PriceBody = false;
            type.PriceFactor.Add((int)TypeCoffee.Type.Instant, 1.5);
            type.PriceFactor.Add((int)TypeCoffee.Type.Granulated, 1.7);
            parameters.Add(type);

            Layers.Add(new Layers.Fluid(5, ColorTranslator.FromHtml("#744F3D")));
            Layers.Add(new Layers.Fluid(2, ColorTranslator.FromHtml("#FFFFFF")));
        }
    }
}
