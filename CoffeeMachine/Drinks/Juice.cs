﻿using Coffee.DrinkParameters;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Coffee.Drinks
{
    class Juice : Drink
    {
        TypeJuice type;
        Ice ice;
        Volume volume;

        const uint basePrice = 10;
        const string name = "Сок";

        public Juice() : base(name, basePrice)
        {
            type = new TypeJuice(TypeJuice.Type.Apple);
            type.PriceBody = true;
            type.PriceFactor.Add((int)TypeJuice.Type.Apple, 0);
            type.PriceFactor.Add((int)TypeJuice.Type.Grape, 0);
            type.PriceFactor.Add((int)TypeJuice.Type.Orange, 0);
            type.PriceFactor.Add((int)TypeJuice.Type.Tomato, 0);
            type.Change += Update_Layers_Change;
            parameters.Add(type);

            ice = new Ice(Ice.IceEnum.WithIce);
            ice.PriceBody = true;
            ice.PriceFactor.Add((int)Ice.IceEnum.WithoutIce, 0);
            ice.PriceFactor.Add((int)Ice.IceEnum.WithIce, 0.5);
            ice.Change += Update_Layers_Change;
            parameters.Add(ice);

            volume = new Volume(Volume.VolumeEnum.Small);
            volume.PriceBody = false;
            volume.PriceFactor.Add((int)Volume.VolumeEnum.Small, 1);
            volume.PriceFactor.Add((int)Volume.VolumeEnum.Medium, 1.2);
            volume.PriceFactor.Add((int)Volume.VolumeEnum.Large, 1.4);
            parameters.Add(volume);

            Layers.Add(new Layers.Fluid(5, ColorTranslator.FromHtml("#DEF2FB")));
        }

        private void Update_Layers_Change(object sender)
        {
            Layers.Clear();
            double fluidDuration = 5;
            switch (type.State)
            {
                case TypeJuice.Type.Apple:
                    Layers.Add(new Layers.Fluid(fluidDuration, ColorTranslator.FromHtml("#66CD4D")));
                    break;
                case TypeJuice.Type.Orange:
                    Layers.Add(new Layers.Fluid(fluidDuration, ColorTranslator.FromHtml("#E78C22")));
                    break;
                case TypeJuice.Type.Tomato:
                    Layers.Add(new Layers.Fluid(fluidDuration, ColorTranslator.FromHtml("#E73922")));
                    break;
                case TypeJuice.Type.Grape:
                    Layers.Add(new Layers.Fluid(fluidDuration, ColorTranslator.FromHtml("#BAED7A")));
                    break;
                default:
                    break;
            }
            if (ice.Exists == Ice.IceEnum.WithIce) Layers.Add(new Layers.Ice(2));
            ParameterChange(this);
        }
    }
}
