﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Coffee
{
    abstract public class DrinkParameter
    {
        public delegate void DrinkParameterEvent (object sender);
        public event DrinkParameterEvent Change;
        protected bool isChangeable = true;
        public string Name
        {
            get;
        }
        protected int state;
        protected Dictionary<int, string> titleTable;

        public bool PriceBody
        {
            get;
            set;
        }
        public Dictionary<int, double> PriceFactor
        {
            get;
            set;
        }

        protected DrinkParameter(string name)
        {
            Name = name;
            titleTable = new Dictionary<int, string>();
            PriceFactor = new Dictionary<int, double>();
        }

        public List<string> getNameValues()
        {
            List<string> result = new List<string>();
            foreach (var item in titleTable.Values)
            {
                result.Add(item.ToString());
            }
            return result;
        }

        public int getCurrentValue()
        {
            return state;
        }

        public string getNameCurrentValue()
        {
            return titleTable[state];
        }

        public void setValue(string valueName)
        {
            foreach (var item in titleTable.Keys)
            {
                if (titleTable[item].ToString() == valueName)
                {
                    state = (int)item;
                    ParameterChange(this);
                }
            }
        }

        protected void ParameterChange(object obj)
        {
            if (Change != null) Change(obj);
        }
    }
}
