﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Coffee
{
    abstract public class DrinkLayer
    {
        public double Duration
        {
            get;
            set;
        }

        public DrinkLayer()
        {
            Duration = 0;
        }
    }
}
