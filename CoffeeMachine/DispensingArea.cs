﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Resources;
using System.Text;
using System.Windows.Forms;

namespace Coffee
{
    public partial class DispensingArea : UserControl
    {
        public delegate void DispensingAreaAnimationEvent(bool cup);
        public event DispensingAreaAnimationEvent Take;
        public event DispensingAreaAnimationEvent AnimationEnd;

        bool cup = false;
        public bool Cup
        {
            get => cup;
            set
            {
                cup = value;
            }
        }

        uint positionPercentage = 0;
        uint PositionPercentage
        {
            get => positionPercentage;
            set
            {
                if (value > 100) positionPercentage = 100;
                else positionPercentage = value;
            }
        }
        uint positionPercentageStop = 0;
        uint PositionPercentageStop
        {
            get => positionPercentageStop;
            set
            {
                if (value > 100) positionPercentageStop = 100;
                else positionPercentageStop = value;
            }
        }

        Bitmap imageBack;

        public DispensingArea()
        {
            InitializeComponent();
            imageBack = Properties.Resources.DispensingArea;

            Refresh();

        }

        public void RefreshDispensingArea()
        {
            Graphics graphicsPaintSpace = this.CreateGraphics();
            BufferedGraphicsContext bufferedGraphicsContext = BufferedGraphicsManager.Current;
            BufferedGraphics bufferedGraphics = bufferedGraphicsContext.Allocate(graphicsPaintSpace, this.DisplayRectangle);
            Graphics gPaint = bufferedGraphics.Graphics;

            gPaint.Clear(BackColor);
            gPaint.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            int widthBack = ClientRectangle.Width;
            int heightBack = ClientRectangle.Height;
            ImageAttributes wrapMode = new ImageAttributes();
            wrapMode.SetWrapMode(WrapMode.Clamp);
            gPaint.DrawImage(imageBack, new Rectangle(0, 0, widthBack, heightBack), 0, 0, imageBack.Width, imageBack.Height, GraphicsUnit.Pixel, wrapMode);

            if (PositionPercentage != 0 && PositionPercentage != 100 && cup)
            {
                RefreshCup(gPaint);
            }
            bufferedGraphics.Render(graphicsPaintSpace);
            gPaint.Dispose();
        }

        public void RefreshCup(Graphics gPaint)
        {
            ImageAttributes wrapMode = new ImageAttributes();
            wrapMode.SetWrapMode(WrapMode.Clamp);
            Bitmap imageCup = global::Coffee.Properties.Resources.Сup;
            int widthBack = ClientRectangle.Width;
            int heightBack = ClientRectangle.Height;
            float ratioCup = ((float)imageCup.Width) / imageCup.Height;
            int heightCup = heightBack / 2;
            int widthCup = (int)(heightCup * ratioCup);
            int xCup = (widthBack) - ((int)(((widthBack + widthCup) * positionPercentage / 100)));
            int yCup = heightBack - heightCup;
            Rectangle rectangleViewCup = new Rectangle(xCup, yCup, widthCup, heightCup);
            gPaint.DrawImage(imageCup, rectangleViewCup, 0, 0, imageCup.Width, imageCup.Height, GraphicsUnit.Pixel, wrapMode);
        }

        public bool TakeCup()
        {
            if (!cup) return false;
            cup = false;
            if (Take != null) Take(cup);
            Refresh();
            return true;
        }

        public void PullOutCup()
        {
            if (cup)
            {
                PutAwayCup();
                AnimationEnd += DispensingArea_AnimationEnd_PullOutCup;
            }
            else
            {
                cup = true;
                AnimationEnd -= DispensingArea_AnimationEnd_PullOutCup;
                PositionPercentage = 0;
                PositionPercentageStop = 50;
                timerAnimate.Enabled = true;
            }
        }

        private void DispensingArea_AnimationEnd_PullOutCup(bool cup)
        {
            this.cup = false;
            PullOutCup();
        }

        public void PutAwayCup()
        {
            if (PositionPercentage != 100)
            {
                PositionPercentageStop = 100;
                timerAnimate.Enabled = true;
            }
            else
            {
                PositionPercentage = 0;
                cup = false;
            }
        }

        private void timerAnimate_Tick(object sender, EventArgs e)
        {
            if (PositionPercentage < PositionPercentageStop) positionPercentage++;
            else if (PositionPercentage < PositionPercentageStop) positionPercentage--;
            else
            {
                if (PositionPercentage == 0 || PositionPercentage == 100) cup = false;
                else cup = true;
                timerAnimate.Enabled = false;
                if (AnimationEnd != null) AnimationEnd(cup);
            }
            Refresh();
        }

        private void DispensingArea_Click(object sender, EventArgs e)
        {
            if (!timerAnimate.Enabled) TakeCup();
        }

        private void DispensingArea_Paint(object sender, PaintEventArgs e)
        {
            RefreshDispensingArea();
        }

    }
}
