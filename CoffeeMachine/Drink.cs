﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Coffee
{
    abstract public class Drink : IDisplayDrinkParametrs, IDrinkCooking
    {
        public delegate void DrinkEvent(object sender);
        public event DrinkEvent Change;

        protected void ParameterChange(object obj)
        {
            Change(obj);
        }

        private List<DrinkLayer> layers;
        protected List<DrinkLayer> Layers
        {
            get => layers;
            set
            {
                layers = value;
                refreshDuration();
            }
        }
        private double duration;
        protected List<DrinkParameter> parameters;
        public string Name
        {
            get;
        }

        protected uint bodyPrice;
        public uint Price
        {
            get => getPrice();
        }

        public Drink(string name, uint bodyPrice)
        {
            Name = name;
            this.bodyPrice = bodyPrice;
            parameters = new List<DrinkParameter>();
            Layers = new List<DrinkLayer>();
        }

        private void refreshDuration()
        {
            duration = 0;
            foreach (DrinkLayer item in Layers)
            {
                duration += item.Duration;
            }
        }

        public List<string> getNameParametrs()
        {
            List<string> result = new List<string>();
            foreach (DrinkParameter item in parameters)
            {
                result.Add(item.Name);
            }
            return result;
        }

        public List<string> getNameValuesParametr(string name)
        {
            DrinkParameter parameter = null;

            foreach (DrinkParameter item in parameters)
            {
                if (item.Name == name)
                {
                    parameter = item;
                    break;
                }
            }
            if (parameter == null) return null;
            return parameter.getNameValues(); ;
        }

        public string getName()
        {
            return Name;
        }

        public List<string> getNameCurrentValuesParametrs()
        {
            List<string> result = new List<string>();
            foreach (DrinkParameter item in parameters)
            {
                result.Add(item.getNameCurrentValue());
            }
            return result;
        }

        public string getNameCurrentValueParametr(string nameParam)
        {
            foreach (DrinkParameter item in parameters)
            {
                if (item.Name == nameParam) return item.getNameCurrentValue();
            }
            return string.Empty;
        }

        public void setValueParametr(string nameParam, string nameValue)
        {
            foreach (DrinkParameter item in parameters)
            {
                if (item.Name == nameParam)
                {
                    item.setValue(nameValue);
                    break;
                }
            }
        }

        protected uint getPrice()
        {
            double price = bodyPrice;
            double priceMultiplier = 1;

            foreach (DrinkParameter item in parameters)
            {
                if (item.PriceBody) price += item.PriceFactor[item.getCurrentValue()];
                else priceMultiplier = priceMultiplier * item.PriceFactor[item.getCurrentValue()];
            }
            price = price * priceMultiplier;
            if (price < 0) throw new Exception("Invalid price");
            return (uint)Math.Round(price);
        }

        public List<DrinkLayer> getDrinkLayers()
        {
            return layers;
        }

        public double getDurationCooking()
        {
            refreshDuration();
            return duration;
        }
    }
}
