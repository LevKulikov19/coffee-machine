﻿
namespace Coffee
{
    partial class CookingArea
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timerAnimateMove = new System.Windows.Forms.Timer(this.components);
            this.timerLayers = new System.Windows.Forms.Timer(this.components);
            this.timerIce = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // timerAnimateMove
            // 
            this.timerAnimateMove.Interval = 33;
            this.timerAnimateMove.Tick += new System.EventHandler(this.timerAnimateMove_Tick);
            // 
            // timerLayers
            // 
            this.timerLayers.Tick += new System.EventHandler(this.timerLayers_Tick);
            // 
            // timerIce
            // 
            this.timerIce.Interval = 33;
            this.timerIce.Tick += new System.EventHandler(this.timerIce_Tick);
            // 
            // CookingArea
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "CookingArea";
            this.Size = new System.Drawing.Size(123, 76);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.CookingArea_Paint);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timerAnimateMove;
        private System.Windows.Forms.Timer timerLayers;
        private System.Windows.Forms.Timer timerIce;
    }
}
