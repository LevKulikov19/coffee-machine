﻿namespace Coffee
{
    partial class DisplayChoiceDrink
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.ButtonWater = new Coffee.Display.DisplayButton();
            this.ButtonJuice = new Coffee.Display.DisplayButton();
            this.ButtonAmericano = new Coffee.Display.DisplayButton();
            this.ButtonСappuccino = new Coffee.Display.DisplayButton();
            this.ButtonHotChocolate = new Coffee.Display.DisplayButton();
            this.ButtonCoffeeWithCream = new Coffee.Display.DisplayButton();
            this.ButtonCoffeeWithSugar = new Coffee.Display.DisplayButton();
            this.ButtonCoffee = new Coffee.Display.DisplayButton();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.ButtonWater, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.ButtonJuice, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.ButtonAmericano, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.ButtonСappuccino, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.ButtonHotChocolate, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.ButtonCoffeeWithCream, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.ButtonCoffeeWithSugar, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.ButtonCoffee, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 10;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 12F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 12F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 12F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(130, 187);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // ButtonWater
            // 
            this.ButtonWater.Background = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(187)))), ((int)(((byte)(231)))));
            this.ButtonWater.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(113)))), ((int)(((byte)(40)))), ((int)(((byte)(34)))));
            this.ButtonWater.CornerRadius = 50;
            this.ButtonWater.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.ButtonWater.ForeColor = System.Drawing.Color.White;
            this.ButtonWater.Location = new System.Drawing.Point(2, 166);
            this.ButtonWater.Margin = new System.Windows.Forms.Padding(2);
            this.ButtonWater.Name = "ButtonWater";
            this.ButtonWater.RoundCorners = ((Coffee.Display.DisplayButton.Corners)((((Coffee.Display.DisplayButton.Corners.TopLeft | Coffee.Display.DisplayButton.Corners.TopRight) 
            | Coffee.Display.DisplayButton.Corners.BottomLeft) 
            | Coffee.Display.DisplayButton.Corners.BottomRight)));
            this.ButtonWater.Size = new System.Drawing.Size(126, 17);
            this.ButtonWater.TabIndex = 8;
            this.ButtonWater.TextButton = "Вода";
            this.ButtonWater.Click += new System.EventHandler(this.ButtonWater_Click);
            // 
            // ButtonJuice
            // 
            this.ButtonJuice.Background = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(187)))), ((int)(((byte)(231)))));
            this.ButtonJuice.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(113)))), ((int)(((byte)(40)))), ((int)(((byte)(34)))));
            this.ButtonJuice.CornerRadius = 50;
            this.ButtonJuice.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.ButtonJuice.ForeColor = System.Drawing.Color.White;
            this.ButtonJuice.Location = new System.Drawing.Point(2, 145);
            this.ButtonJuice.Margin = new System.Windows.Forms.Padding(2);
            this.ButtonJuice.Name = "ButtonJuice";
            this.ButtonJuice.RoundCorners = ((Coffee.Display.DisplayButton.Corners)((((Coffee.Display.DisplayButton.Corners.TopLeft | Coffee.Display.DisplayButton.Corners.TopRight) 
            | Coffee.Display.DisplayButton.Corners.BottomLeft) 
            | Coffee.Display.DisplayButton.Corners.BottomRight)));
            this.ButtonJuice.Size = new System.Drawing.Size(126, 17);
            this.ButtonJuice.TabIndex = 7;
            this.ButtonJuice.TextButton = "Сок";
            this.ButtonJuice.Click += new System.EventHandler(this.ButtonJuice_Click);
            // 
            // ButtonAmericano
            // 
            this.ButtonAmericano.Background = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(187)))), ((int)(((byte)(231)))));
            this.ButtonAmericano.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(113)))), ((int)(((byte)(40)))), ((int)(((byte)(34)))));
            this.ButtonAmericano.CornerRadius = 50;
            this.ButtonAmericano.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.ButtonAmericano.ForeColor = System.Drawing.Color.White;
            this.ButtonAmericano.Location = new System.Drawing.Point(2, 124);
            this.ButtonAmericano.Margin = new System.Windows.Forms.Padding(2);
            this.ButtonAmericano.Name = "ButtonAmericano";
            this.ButtonAmericano.RoundCorners = ((Coffee.Display.DisplayButton.Corners)((((Coffee.Display.DisplayButton.Corners.TopLeft | Coffee.Display.DisplayButton.Corners.TopRight) 
            | Coffee.Display.DisplayButton.Corners.BottomLeft) 
            | Coffee.Display.DisplayButton.Corners.BottomRight)));
            this.ButtonAmericano.Size = new System.Drawing.Size(126, 17);
            this.ButtonAmericano.TabIndex = 6;
            this.ButtonAmericano.TextButton = "Американо";
            this.ButtonAmericano.Click += new System.EventHandler(this.ButtonAmericano_Click);
            // 
            // ButtonСappuccino
            // 
            this.ButtonСappuccino.Background = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(187)))), ((int)(((byte)(231)))));
            this.ButtonСappuccino.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(113)))), ((int)(((byte)(40)))), ((int)(((byte)(34)))));
            this.ButtonСappuccino.CornerRadius = 50;
            this.ButtonСappuccino.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.ButtonСappuccino.ForeColor = System.Drawing.Color.White;
            this.ButtonСappuccino.Location = new System.Drawing.Point(2, 103);
            this.ButtonСappuccino.Margin = new System.Windows.Forms.Padding(2);
            this.ButtonСappuccino.Name = "ButtonСappuccino";
            this.ButtonСappuccino.RoundCorners = ((Coffee.Display.DisplayButton.Corners)((((Coffee.Display.DisplayButton.Corners.TopLeft | Coffee.Display.DisplayButton.Corners.TopRight) 
            | Coffee.Display.DisplayButton.Corners.BottomLeft) 
            | Coffee.Display.DisplayButton.Corners.BottomRight)));
            this.ButtonСappuccino.Size = new System.Drawing.Size(126, 17);
            this.ButtonСappuccino.TabIndex = 5;
            this.ButtonСappuccino.TextButton = "Капучино";
            this.ButtonСappuccino.Click += new System.EventHandler(this.ButtonСappuccino_Click);
            // 
            // ButtonHotChocolate
            // 
            this.ButtonHotChocolate.Background = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(187)))), ((int)(((byte)(231)))));
            this.ButtonHotChocolate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(113)))), ((int)(((byte)(40)))), ((int)(((byte)(34)))));
            this.ButtonHotChocolate.CornerRadius = 50;
            this.ButtonHotChocolate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.ButtonHotChocolate.ForeColor = System.Drawing.Color.White;
            this.ButtonHotChocolate.Location = new System.Drawing.Point(2, 82);
            this.ButtonHotChocolate.Margin = new System.Windows.Forms.Padding(2);
            this.ButtonHotChocolate.Name = "ButtonHotChocolate";
            this.ButtonHotChocolate.RoundCorners = ((Coffee.Display.DisplayButton.Corners)((((Coffee.Display.DisplayButton.Corners.TopLeft | Coffee.Display.DisplayButton.Corners.TopRight) 
            | Coffee.Display.DisplayButton.Corners.BottomLeft) 
            | Coffee.Display.DisplayButton.Corners.BottomRight)));
            this.ButtonHotChocolate.Size = new System.Drawing.Size(126, 17);
            this.ButtonHotChocolate.TabIndex = 4;
            this.ButtonHotChocolate.TextButton = "Горячий шоколад";
            this.ButtonHotChocolate.Click += new System.EventHandler(this.ButtonHotChocolate_Click);
            // 
            // ButtonCoffeeWithCream
            // 
            this.ButtonCoffeeWithCream.Background = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(187)))), ((int)(((byte)(231)))));
            this.ButtonCoffeeWithCream.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(113)))), ((int)(((byte)(40)))), ((int)(((byte)(34)))));
            this.ButtonCoffeeWithCream.CornerRadius = 50;
            this.ButtonCoffeeWithCream.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.ButtonCoffeeWithCream.ForeColor = System.Drawing.Color.White;
            this.ButtonCoffeeWithCream.Location = new System.Drawing.Point(2, 61);
            this.ButtonCoffeeWithCream.Margin = new System.Windows.Forms.Padding(2);
            this.ButtonCoffeeWithCream.Name = "ButtonCoffeeWithCream";
            this.ButtonCoffeeWithCream.RoundCorners = ((Coffee.Display.DisplayButton.Corners)((((Coffee.Display.DisplayButton.Corners.TopLeft | Coffee.Display.DisplayButton.Corners.TopRight) 
            | Coffee.Display.DisplayButton.Corners.BottomLeft) 
            | Coffee.Display.DisplayButton.Corners.BottomRight)));
            this.ButtonCoffeeWithCream.Size = new System.Drawing.Size(126, 17);
            this.ButtonCoffeeWithCream.TabIndex = 3;
            this.ButtonCoffeeWithCream.TextButton = "Кофе со сливками";
            this.ButtonCoffeeWithCream.Click += new System.EventHandler(this.ButtonCoffeeWithCream_Click);
            // 
            // ButtonCoffeeWithSugar
            // 
            this.ButtonCoffeeWithSugar.Background = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(187)))), ((int)(((byte)(231)))));
            this.ButtonCoffeeWithSugar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(113)))), ((int)(((byte)(40)))), ((int)(((byte)(34)))));
            this.ButtonCoffeeWithSugar.CornerRadius = 50;
            this.ButtonCoffeeWithSugar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.ButtonCoffeeWithSugar.ForeColor = System.Drawing.Color.White;
            this.ButtonCoffeeWithSugar.Location = new System.Drawing.Point(2, 40);
            this.ButtonCoffeeWithSugar.Margin = new System.Windows.Forms.Padding(2);
            this.ButtonCoffeeWithSugar.Name = "ButtonCoffeeWithSugar";
            this.ButtonCoffeeWithSugar.RoundCorners = ((Coffee.Display.DisplayButton.Corners)((((Coffee.Display.DisplayButton.Corners.TopLeft | Coffee.Display.DisplayButton.Corners.TopRight) 
            | Coffee.Display.DisplayButton.Corners.BottomLeft) 
            | Coffee.Display.DisplayButton.Corners.BottomRight)));
            this.ButtonCoffeeWithSugar.Size = new System.Drawing.Size(126, 17);
            this.ButtonCoffeeWithSugar.TabIndex = 2;
            this.ButtonCoffeeWithSugar.TextButton = "Кофе с сахаром";
            this.ButtonCoffeeWithSugar.Click += new System.EventHandler(this.ButtonCoffeeWithSugar_Click);
            // 
            // ButtonCoffee
            // 
            this.ButtonCoffee.Background = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(187)))), ((int)(((byte)(231)))));
            this.ButtonCoffee.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(113)))), ((int)(((byte)(40)))), ((int)(((byte)(34)))));
            this.ButtonCoffee.CornerRadius = 50;
            this.ButtonCoffee.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.ButtonCoffee.ForeColor = System.Drawing.Color.White;
            this.ButtonCoffee.Location = new System.Drawing.Point(2, 19);
            this.ButtonCoffee.Margin = new System.Windows.Forms.Padding(2);
            this.ButtonCoffee.Name = "ButtonCoffee";
            this.ButtonCoffee.RoundCorners = ((Coffee.Display.DisplayButton.Corners)((((Coffee.Display.DisplayButton.Corners.TopLeft | Coffee.Display.DisplayButton.Corners.TopRight) 
            | Coffee.Display.DisplayButton.Corners.BottomLeft) 
            | Coffee.Display.DisplayButton.Corners.BottomRight)));
            this.ButtonCoffee.Size = new System.Drawing.Size(126, 17);
            this.ButtonCoffee.TabIndex = 1;
            this.ButtonCoffee.TextButton = "Кофе";
            this.ButtonCoffee.Click += new System.EventHandler(this.ButtonCoffee_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(2, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Выбирите напиток";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // DisplayChoiceDrink
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(187)))), ((int)(((byte)(231)))));
            this.Controls.Add(this.tableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "DisplayChoiceDrink";
            this.Size = new System.Drawing.Size(130, 187);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private Coffee.Display.DisplayButton ButtonCoffee;
        private Coffee.Display.DisplayButton ButtonWater;
        private Coffee.Display.DisplayButton ButtonJuice;
        private Coffee.Display.DisplayButton ButtonAmericano;
        private Coffee.Display.DisplayButton ButtonСappuccino;
        private Coffee.Display.DisplayButton ButtonHotChocolate;
        private Coffee.Display.DisplayButton ButtonCoffeeWithCream;
        private Coffee.Display.DisplayButton ButtonCoffeeWithSugar;
    }
}
