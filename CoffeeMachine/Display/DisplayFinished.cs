﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Coffee
{
    public partial class DisplayFinished : UserControl
    {
        internal delegate void DisplayFinishedEvent(object sender);
        internal event DisplayFinishedEvent Start;
        public DisplayFinished()
        {
            InitializeComponent();
        }

        private void DisplayFinished_Click(object sender, EventArgs e)
        {
            Start(this);
        }

        private void label1_Click(object sender, EventArgs e)
        {
            Start(this);
        }
    }
}
