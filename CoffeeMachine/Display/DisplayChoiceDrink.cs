﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Coffee
{
    public partial class DisplayChoiceDrink : UserControl
    {
        public delegate void DisplayChoiceDrinkEvent(object sender, Drink drink);
        public event DisplayChoiceDrinkEvent ChoiceDrink;
        public DisplayChoiceDrink()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void ButtonCoffee_Click(object sender, EventArgs e)
        {
            ChoiceDrink(this, new Drinks.Coffee());
        }

        private void ButtonCoffeeWithSugar_Click(object sender, EventArgs e)
        {
            ChoiceDrink(this, new Drinks.CoffeeWithSugar());
        }

        private void ButtonCoffeeWithCream_Click(object sender, EventArgs e)
        {
            ChoiceDrink(this, new Drinks.CoffeeWithCream());
        }

        private void ButtonHotChocolate_Click(object sender, EventArgs e)
        {
            ChoiceDrink(this, new Drinks.HotСhocolate());
        }

        private void ButtonСappuccino_Click(object sender, EventArgs e)
        {
            ChoiceDrink(this, new Drinks.Cappuccino());
        }

        private void ButtonAmericano_Click(object sender, EventArgs e)
        {
            ChoiceDrink(this, new Drinks.Americano());
        }

        private void ButtonJuice_Click(object sender, EventArgs e)
        {
            ChoiceDrink(this, new Drinks.Juice());
        }

        private void ButtonWater_Click(object sender, EventArgs e)
        {
            ChoiceDrink(this, new Drinks.Water());
        }
    }
}
