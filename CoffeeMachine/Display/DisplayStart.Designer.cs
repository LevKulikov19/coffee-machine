﻿
using Coffee.Display;
using System.Drawing;

namespace Coffee
{
    partial class DisplayStart
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.displayButton = new Coffee.Display.DisplayButton();
            this.SuspendLayout();
            // 
            // displayButton
            // 
            this.displayButton.Background = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(187)))), ((int)(((byte)(231)))));
            this.displayButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(113)))), ((int)(((byte)(40)))), ((int)(((byte)(34)))));
            this.displayButton.CornerRadius = 50;
            this.displayButton.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.displayButton.ForeColor = System.Drawing.Color.White;
            this.displayButton.Location = new System.Drawing.Point(0, 77);
            this.displayButton.Margin = new System.Windows.Forms.Padding(2);
            this.displayButton.Name = "displayButtonMain1";
            this.displayButton.RoundCorners = ((Coffee.Display.DisplayButton.Corners)((((Coffee.Display.DisplayButton.Corners.TopLeft | Coffee.Display.DisplayButton.Corners.TopRight) 
            | Coffee.Display.DisplayButton.Corners.BottomLeft) 
            | Coffee.Display.DisplayButton.Corners.BottomRight)));
            this.displayButton.Size = new System.Drawing.Size(130, 26);
            this.displayButton.TabIndex = 0;
            this.displayButton.TextButton = "Начать";
            this.displayButton.Click += new System.EventHandler(this.displayButtonMain1_Click);
            // 
            // DisplayStart
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(187)))), ((int)(((byte)(231)))));
            this.Controls.Add(this.displayButton);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "DisplayStart";
            this.Size = new System.Drawing.Size(130, 187);
            this.ResumeLayout(false);

        }

        #endregion

        private Coffee.Display.DisplayButton displayButton;
    }
}
