﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Coffee
{
    public partial class DisplayCooking : UserControl
    {
        uint completionPercentage;
        public uint CompletionPercentage
        {
            get => completionPercentage;
            set
            {
                if (value > 100) completionPercentage = 100;
                else completionPercentage = value;
                ProgressBar.Value = completionPercentage;
            }
        }

        public Drink Drink
        {
            get;
        }

        public DisplayCooking(Drink drink = null)
        {
            InitializeComponent();
            Drink = drink;
            InitializeLableDrink(drink);
        }

        private void InitializeLableDrink(Drink drink)
        {
            if (drink == null)
            {
                labelParam1.Text = string.Empty;
                labelParam2.Text = string.Empty;
                labelParam3.Text = string.Empty;
                labelName.Text = string.Empty;
                return;
            }

            labelName.Text = drink.Name;
            List<string> paramValue = drink.getNameCurrentValuesParametrs();
            if (paramValue.Count >= 1) labelParam1.Text = paramValue[0];
            if (paramValue.Count >= 2) labelParam2.Text = paramValue[1];
            if (paramValue.Count >= 3) labelParam3.Text = paramValue[2];

        }
    }
}
