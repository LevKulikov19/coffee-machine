﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Coffee
{
    public partial class DisplayDrinkParameters : UserControl
    {
        public delegate void DisplayDrinkParametersEvent(object sender, Drink drink);
        public event DisplayDrinkParametersEvent Back;
        public event DisplayDrinkParametersEvent Payment;

        public Drink Drink
        {
            get;
        }

        private List<string> param1Values;
        private int param1CurrentIndex;
        private List<string> param2Values;
        private int param2CurrentIndex;
        private List<string> param3Values;
        private int param3CurrentIndex;

        public DisplayDrinkParameters()
        {
            InitializeComponent();

        }

        public DisplayDrinkParameters(Drink drink = null)
        {
            InitializeComponent();
            param1Values = new List<string>();
            param2Values = new List<string>();
            param3Values = new List<string>();

            param1CurrentIndex = -1;
            param2CurrentIndex = -1;
            param3CurrentIndex = -1;

            Drink = drink;
            drink.Change += Drink_Change;
            displayDrinkCup1.setDrinkLayers(drink);
            InitializeLableDrink(drink);
            RefreshLableDrink();
        }

        private void Drink_Change(object sender)
        {
            displayDrinkCup1.setDrinkLayers(Drink);
        }

        private void InitializeLableDrink(Drink drink)
        {
            List<string> parameters = Drink.getNameParametrs();

            ButtonArrowBackParam1.View = false;
            ButtonArrowBackParam2.View = false;
            ButtonArrowBackParam3.View = false;

            ButtonArrowPrevParam1.View = false;
            ButtonArrowPrevParam2.View = false;
            ButtonArrowPrevParam3.View = false;

            labelParam1.Text = string.Empty;
            labelParam2.Text = string.Empty;
            labelParam3.Text = string.Empty;

            if (parameters.Count >= 1)
            {
                ButtonArrowBackParam1.View = true;
                ButtonArrowBackParam1.Disable = true;
                ButtonArrowPrevParam1.View = true;
                param1CurrentIndex = 0;
                param1Values = drink.getNameValuesParametr(parameters[0]);
                if (param1Values.Count <= 1) ButtonArrowPrevParam1.Disable = true;
                string defalt1Value = drink.getNameCurrentValueParametr(parameters[0]);
                for (int i = 0; i < param1Values.Count; i++)
                {
                    if (defalt1Value == param1Values[i])
                    {
                        param1CurrentIndex = i;
                        break;
                    }
                }
                labelParam1.Text = param1Values[param1CurrentIndex];
            }
            if (parameters.Count >= 2)
            {
                ButtonArrowBackParam2.View = true;
                ButtonArrowBackParam2.Disable = true;
                ButtonArrowPrevParam2.View = true;
                param2CurrentIndex = 0;
                param2Values = drink.getNameValuesParametr(parameters[1]);
                if (param2Values.Count <= 2) ButtonArrowPrevParam2.Disable = true;
                string defalt2Value = drink.getNameCurrentValueParametr(parameters[1]);
                for (int i = 0; i < param2Values.Count; i++)
                {
                    if (defalt2Value == param2Values[i])
                    {
                        param2CurrentIndex = i;
                        break;
                    }
                }
                labelParam2.Text = param2Values[param2CurrentIndex];
            }
            if (parameters.Count >= 3)
            {
                ButtonArrowBackParam3.View = true;
                ButtonArrowBackParam3.Disable = true;
                ButtonArrowPrevParam3.View = true;
                param3CurrentIndex = 0;
                param3Values = drink.getNameValuesParametr(parameters[2]);
                if (param3Values.Count <= 1) ButtonArrowPrevParam3.Disable = true;
                string defalt3Value = drink.getNameCurrentValueParametr(parameters[2]);
                for (int i = 0; i < param3Values.Count; i++)
                {
                    if (defalt3Value == param3Values[i])
                    {
                        param3CurrentIndex = i;
                        break;
                    }
                }
                labelParam3.Text = param3Values[param3CurrentIndex];
            }
        }

        private void RefreshLableDrink()
        {
            List<string> parameters = Drink.getNameParametrs();
            if (Drink == null)
            {
                labelParam1.Text = string.Empty;
                labelParam2.Text = string.Empty;
                labelParam3.Text = string.Empty;
                labelName.Text = string.Empty;
                return;
            }

            labelName.Text = Drink.Name;

            if (param1CurrentIndex != -1)
            {
                ButtonArrowBackParam1.View = true;
                ButtonArrowPrevParam1.View = true;

                if (param1CurrentIndex == 0)
                    ButtonArrowBackParam1.Disable = true;
                else
                    ButtonArrowBackParam1.Disable = false;

                if (param1CurrentIndex == param1Values.Count-1)
                    ButtonArrowPrevParam1.Disable = true;
                else
                    ButtonArrowPrevParam1.Disable = false;

                labelParam1.Text = param1Values[param1CurrentIndex];
                Drink.setValueParametr(parameters[0], param1Values[param1CurrentIndex]);

            }
            else
            {
                ButtonArrowBackParam1.View = false;
                labelParam1.Text = string.Empty;
                ButtonArrowPrevParam1.View = false;
            }
            
            if (param2CurrentIndex != -1)
            {
                ButtonArrowBackParam2.View = true;
                ButtonArrowPrevParam2.View = true;

                if (param2CurrentIndex == 0)
                    ButtonArrowBackParam2.Disable = true;
                else
                    ButtonArrowBackParam2.Disable = false;

                if (param2CurrentIndex == param2Values.Count - 1)
                    ButtonArrowPrevParam2.Disable = true;
                else
                    ButtonArrowPrevParam2.Disable = false;

                labelParam2.Text = param2Values[param2CurrentIndex];
                Drink.setValueParametr(parameters[1], param2Values[param2CurrentIndex]);

            }
            else
            {
                ButtonArrowBackParam2.View = false;
                labelParam2.Text = string.Empty;
                ButtonArrowPrevParam2.View = false;
            }

            if (param3CurrentIndex != -1)
            {
                ButtonArrowBackParam3.View = true;
                ButtonArrowPrevParam3.View = true;

                if (param3CurrentIndex == 0)
                    ButtonArrowBackParam3.Disable = true;
                else
                    ButtonArrowBackParam3.Disable = false;

                if (param3CurrentIndex == param3Values.Count - 1)
                    ButtonArrowPrevParam3.Disable = true;
                else
                    ButtonArrowPrevParam3.Disable = false;

                labelParam3.Text = param3Values[param3CurrentIndex];
                Drink.setValueParametr(parameters[2], param3Values[param3CurrentIndex]);

            }
            else
            {
                ButtonArrowBackParam3.View = false;
                labelParam3.Text = string.Empty;
                ButtonArrowPrevParam3.View = false;
            }

            labelPrice.Text = Drink.Price.ToString();

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void ButtonBack_Click(object sender, EventArgs e)
        {
            Back(this, Drink);
        }

        private void ButtonPayment_Click(object sender, EventArgs e)
        {
            Payment(this, Drink);
        }

        private void ButtonArrowBackParam1_Click(object sender, EventArgs e)
        {
            if (!ButtonArrowBackParam1.Disable) param1CurrentIndex--;
            RefreshLableDrink();
        }

        private void ButtonArrowBackParam2_Click(object sender, EventArgs e)
        {
            if (!ButtonArrowBackParam2.Disable) param2CurrentIndex--;
            RefreshLableDrink();
        }

        private void ButtonArrowBackParam3_Click(object sender, EventArgs e)
        {
            if (!ButtonArrowBackParam3.Disable) param3CurrentIndex--;
            RefreshLableDrink();
        }

        private void ButtonArrowPrevParam1_Click(object sender, EventArgs e)
        {
            if (!ButtonArrowPrevParam1.Disable) param1CurrentIndex++;
            RefreshLableDrink();
        }

        private void ButtonArrowPrevParam2_Click(object sender, EventArgs e)
        {
            if (!ButtonArrowPrevParam2.Disable) param2CurrentIndex++;
            RefreshLableDrink();
        }

        private void ButtonArrowPrevParam3_Click(object sender, EventArgs e)
        {
            if (!ButtonArrowPrevParam3.Disable) param3CurrentIndex++;
            RefreshLableDrink();
        }

        private void ButtonArrowBackParam3_Load(object sender, EventArgs e)
        {

        }

        private void labelParam1_Click(object sender, EventArgs e)
        {

        }
    }
}
