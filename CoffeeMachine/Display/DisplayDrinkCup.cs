using Coffee.Layers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;
using System.Windows.Forms;

namespace Coffee.Display
{
    public partial class DisplayDrinkCup : UserControl
    {
        private List<DrinkLayer> layers;
        public DisplayDrinkCup()
        {
            InitializeComponent();
            layers = new List<DrinkLayer>();
            RefreshComp();
        }

        private void DisplayDrinkCup_Paint(object sender, PaintEventArgs e)
        {
            RefreshComp();
        }

        void RefreshComp()
        {
            Graphics graphicsPaintSpace = this.CreateGraphics();
            BufferedGraphicsContext bufferedGraphicsContext = BufferedGraphicsManager.Current;
            BufferedGraphics bufferedGraphics = bufferedGraphicsContext.Allocate(graphicsPaintSpace, this.DisplayRectangle);
            Graphics gPaint = bufferedGraphics.Graphics;

            gPaint.Clear(BackColor);
            gPaint.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            Brush brushBase = new SolidBrush(ColorTranslator.FromHtml("#D6C1BF"));
            Rectangle rectangleBase = ClientRectangle;
            gPaint.FillRectangle(brushBase, rectangleBase);

            int topMargin = 5;
            double durationAllLayers = 0;
            bool isAddIce = false;

            foreach (DrinkLayer item in layers)
            {
                if (item is Fluid) durationAllLayers += item.Duration;
                if (item is Ice)
                {
                    isAddIce = true;
                }
            }
            if (isAddIce)
            {
                Bitmap imageIce = global::Coffee.Properties.Resources.ice;
                int widthIce = 10;
                int heightICe = widthIce;
                int xIce = ClientRectangle.Width / 2 - widthIce / 2;
                int yIce = 0;
                Rectangle rectangleViewIce = new Rectangle(xIce, yIce, widthIce, heightICe);
                ImageAttributes wrapMode = new ImageAttributes();
                gPaint.DrawImage(imageIce, rectangleViewIce, 0, 0, imageIce.Width, imageIce.Height, GraphicsUnit.Pixel, wrapMode);
                imageIce.Dispose();
            }

            layers.Reverse();
            foreach (DrinkLayer item in layers)
            {
                if (item is Fluid)
                {
                    Fluid fluid = item as Fluid;
                    Rectangle rectangleFluid = new Rectangle(0, (int)((rectangleBase.Height) - fluid.Duration / durationAllLayers * (rectangleBase.Height - topMargin)),
                                                             rectangleBase.Width, (int)(fluid.Duration / durationAllLayers * (rectangleBase.Height - topMargin)));
                    Color colorFluid =  Color.FromArgb(190,fluid.MainColor.R, fluid.MainColor.G, fluid.MainColor.B);
                    Brush brushFluid = new SolidBrush(colorFluid);
                    gPaint.FillRectangle(brushFluid, rectangleFluid);
                }
            }
            layers.Reverse();


            //контур 
            int widthStroke = 2;
            Pen pen = new Pen(ColorTranslator.FromHtml("#712822"), widthStroke);
            Point[] pointsStroke = new Point[4];
            pointsStroke[0] = new Point(widthStroke / 2, 0);
            pointsStroke[1] = new Point(widthStroke / 2, this.ClientRectangle.Height - widthStroke);
            pointsStroke[2] = new Point(this.ClientRectangle.Width - widthStroke, this.ClientRectangle.Height - widthStroke);
            pointsStroke[3] = new Point(this.ClientRectangle.Width - widthStroke, 0);
            gPaint.DrawLine(pen, pointsStroke[1], pointsStroke[2]); //нижняя линия
            gPaint.DrawLine(pen, pointsStroke[0], pointsStroke[1]); //левая линия
            gPaint.DrawLine(pen, pointsStroke[2], pointsStroke[3]); //правая линия


            bufferedGraphics.Render(graphicsPaintSpace);
        }

        public void setDrinkLayers(IDrinkCooking drink)
        {
            layers = drink.getDrinkLayers();
            if (this.IsDisposed != true) RefreshComp();
        }
    }
}
