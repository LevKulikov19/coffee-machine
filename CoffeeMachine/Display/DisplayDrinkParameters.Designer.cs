﻿
using System.Drawing;

namespace Coffee
{
    partial class DisplayDrinkParameters
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.ButtonBack = new Coffee.Display.DisplayButton();
            this.panelParam3 = new System.Windows.Forms.Panel();
            this.ButtonArrowPrevParam3 = new Coffee.Display.DisplayButtonArrow();
            this.ButtonArrowBackParam3 = new Coffee.Display.DisplayButtonArrow();
            this.labelParam3 = new System.Windows.Forms.Label();
            this.panelParam2 = new System.Windows.Forms.Panel();
            this.ButtonArrowPrevParam2 = new Coffee.Display.DisplayButtonArrow();
            this.ButtonArrowBackParam2 = new Coffee.Display.DisplayButtonArrow();
            this.labelParam2 = new System.Windows.Forms.Label();
            this.labelName = new System.Windows.Forms.Label();
            this.ButtonPayment = new Coffee.Display.DisplayButton();
            this.panelParam1 = new System.Windows.Forms.Panel();
            this.ButtonArrowPrevParam1 = new Coffee.Display.DisplayButtonArrow();
            this.ButtonArrowBackParam1 = new Coffee.Display.DisplayButtonArrow();
            this.labelParam1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.labelPrice = new System.Windows.Forms.Label();
            this.displayDrinkCup1 = new Coffee.Display.DisplayDrinkCup();
            this.tableLayoutPanel1.SuspendLayout();
            this.panelParam3.SuspendLayout();
            this.panelParam2.SuspendLayout();
            this.panelParam1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.ButtonBack, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panelParam3, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.panelParam2, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.labelName, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.ButtonPayment, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.panelParam1, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 6);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 8;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(186, 312);
            this.tableLayoutPanel1.TabIndex = 0;
            this.tableLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint);
            // 
            // ButtonBack
            // 
            this.ButtonBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(113)))), ((int)(((byte)(40)))), ((int)(((byte)(34)))));
            this.ButtonBack.Background = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(187)))), ((int)(((byte)(231)))));
            this.ButtonBack.CornerRadius = 50;
            this.ButtonBack.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.ButtonBack.ForeColor = System.Drawing.Color.White;
            this.ButtonBack.Location = new System.Drawing.Point(3, 3);
            this.ButtonBack.Name = "ButtonBack";
            this.ButtonBack.RoundCorners = Coffee.Display.DisplayButton.Corners.All;
            this.ButtonBack.Size = new System.Drawing.Size(64, 27);
            this.ButtonBack.TabIndex = 0;
            this.ButtonBack.TextButton = "Назад";
            this.ButtonBack.Click += new System.EventHandler(this.ButtonBack_Click);
            // 
            // panelParam3
            // 
            this.panelParam3.Controls.Add(this.ButtonArrowPrevParam3);
            this.panelParam3.Controls.Add(this.ButtonArrowBackParam3);
            this.panelParam3.Controls.Add(this.labelParam3);
            this.panelParam3.Location = new System.Drawing.Point(0, 158);
            this.panelParam3.Margin = new System.Windows.Forms.Padding(0, 2, 0, 2);
            this.panelParam3.Name = "panelParam3";
            this.panelParam3.Size = new System.Drawing.Size(186, 35);
            this.panelParam3.TabIndex = 9;
            // 
            // ButtonArrowPrevParam3
            // 
            this.ButtonArrowPrevParam3.BackColor = System.Drawing.Color.Transparent;
            this.ButtonArrowPrevParam3.Background = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(187)))), ((int)(((byte)(231)))));
            this.ButtonArrowPrevParam3.Disable = false;
            this.ButtonArrowPrevParam3.Dock = System.Windows.Forms.DockStyle.Right;
            this.ButtonArrowPrevParam3.Flip = false;
            this.ButtonArrowPrevParam3.Foreground = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(104)))), ((int)(((byte)(120)))));
            this.ButtonArrowPrevParam3.Location = new System.Drawing.Point(172, 0);
            this.ButtonArrowPrevParam3.Margin = new System.Windows.Forms.Padding(1, 2, 1, 2);
            this.ButtonArrowPrevParam3.Name = "ButtonArrowPrevParam3";
            this.ButtonArrowPrevParam3.Size = new System.Drawing.Size(14, 35);
            this.ButtonArrowPrevParam3.TabIndex = 7;
            this.ButtonArrowPrevParam3.View = true;
            this.ButtonArrowPrevParam3.Click += new System.EventHandler(this.ButtonArrowPrevParam3_Click);
            // 
            // ButtonArrowBackParam3
            // 
            this.ButtonArrowBackParam3.BackColor = System.Drawing.Color.Transparent;
            this.ButtonArrowBackParam3.Background = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(187)))), ((int)(((byte)(231)))));
            this.ButtonArrowBackParam3.Disable = false;
            this.ButtonArrowBackParam3.Dock = System.Windows.Forms.DockStyle.Left;
            this.ButtonArrowBackParam3.Flip = true;
            this.ButtonArrowBackParam3.Foreground = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(104)))), ((int)(((byte)(120)))));
            this.ButtonArrowBackParam3.Location = new System.Drawing.Point(0, 0);
            this.ButtonArrowBackParam3.Margin = new System.Windows.Forms.Padding(1, 2, 1, 2);
            this.ButtonArrowBackParam3.Name = "ButtonArrowBackParam3";
            this.ButtonArrowBackParam3.Size = new System.Drawing.Size(14, 35);
            this.ButtonArrowBackParam3.TabIndex = 6;
            this.ButtonArrowBackParam3.View = true;
            this.ButtonArrowBackParam3.Load += new System.EventHandler(this.ButtonArrowBackParam3_Load);
            this.ButtonArrowBackParam3.Click += new System.EventHandler(this.ButtonArrowBackParam3_Click);
            // 
            // labelParam3
            // 
            this.labelParam3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelParam3.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelParam3.ForeColor = System.Drawing.Color.White;
            this.labelParam3.Location = new System.Drawing.Point(10, 0);
            this.labelParam3.Name = "labelParam3";
            this.labelParam3.Size = new System.Drawing.Size(165, 35);
            this.labelParam3.TabIndex = 5;
            this.labelParam3.Text = "Напиток";
            this.labelParam3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelParam2
            // 
            this.panelParam2.Controls.Add(this.ButtonArrowPrevParam2);
            this.panelParam2.Controls.Add(this.ButtonArrowBackParam2);
            this.panelParam2.Controls.Add(this.labelParam2);
            this.panelParam2.Location = new System.Drawing.Point(0, 119);
            this.panelParam2.Margin = new System.Windows.Forms.Padding(0, 2, 0, 2);
            this.panelParam2.Name = "panelParam2";
            this.panelParam2.Size = new System.Drawing.Size(186, 35);
            this.panelParam2.TabIndex = 8;
            // 
            // ButtonArrowPrevParam2
            // 
            this.ButtonArrowPrevParam2.BackColor = System.Drawing.Color.Transparent;
            this.ButtonArrowPrevParam2.Background = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(187)))), ((int)(((byte)(231)))));
            this.ButtonArrowPrevParam2.Disable = false;
            this.ButtonArrowPrevParam2.Dock = System.Windows.Forms.DockStyle.Right;
            this.ButtonArrowPrevParam2.Flip = false;
            this.ButtonArrowPrevParam2.Foreground = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(104)))), ((int)(((byte)(120)))));
            this.ButtonArrowPrevParam2.Location = new System.Drawing.Point(172, 0);
            this.ButtonArrowPrevParam2.Margin = new System.Windows.Forms.Padding(1, 2, 1, 2);
            this.ButtonArrowPrevParam2.Name = "ButtonArrowPrevParam2";
            this.ButtonArrowPrevParam2.Size = new System.Drawing.Size(14, 35);
            this.ButtonArrowPrevParam2.TabIndex = 7;
            this.ButtonArrowPrevParam2.View = true;
            this.ButtonArrowPrevParam2.Click += new System.EventHandler(this.ButtonArrowPrevParam2_Click);
            // 
            // ButtonArrowBackParam2
            // 
            this.ButtonArrowBackParam2.BackColor = System.Drawing.Color.Transparent;
            this.ButtonArrowBackParam2.Background = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(187)))), ((int)(((byte)(231)))));
            this.ButtonArrowBackParam2.Disable = false;
            this.ButtonArrowBackParam2.Dock = System.Windows.Forms.DockStyle.Left;
            this.ButtonArrowBackParam2.Flip = true;
            this.ButtonArrowBackParam2.Foreground = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(104)))), ((int)(((byte)(120)))));
            this.ButtonArrowBackParam2.Location = new System.Drawing.Point(0, 0);
            this.ButtonArrowBackParam2.Margin = new System.Windows.Forms.Padding(1, 2, 1, 2);
            this.ButtonArrowBackParam2.Name = "ButtonArrowBackParam2";
            this.ButtonArrowBackParam2.Size = new System.Drawing.Size(14, 35);
            this.ButtonArrowBackParam2.TabIndex = 6;
            this.ButtonArrowBackParam2.View = true;
            this.ButtonArrowBackParam2.Click += new System.EventHandler(this.ButtonArrowBackParam2_Click);
            // 
            // labelParam2
            // 
            this.labelParam2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelParam2.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelParam2.ForeColor = System.Drawing.Color.White;
            this.labelParam2.Location = new System.Drawing.Point(10, -2);
            this.labelParam2.Name = "labelParam2";
            this.labelParam2.Size = new System.Drawing.Size(165, 37);
            this.labelParam2.TabIndex = 5;
            this.labelParam2.Text = "Напиток";
            this.labelParam2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelName.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point);
            this.labelName.ForeColor = System.Drawing.Color.White;
            this.labelName.Location = new System.Drawing.Point(0, 39);
            this.labelName.Margin = new System.Windows.Forms.Padding(0);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(186, 28);
            this.labelName.TabIndex = 1;
            this.labelName.Text = "Напиток";
            this.labelName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.labelName.Click += new System.EventHandler(this.label1_Click);
            // 
            // ButtonPayment
            // 
            this.ButtonPayment.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(113)))), ((int)(((byte)(40)))), ((int)(((byte)(34)))));
            this.ButtonPayment.Background = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(187)))), ((int)(((byte)(231)))));
            this.ButtonPayment.CornerRadius = 50;
            this.ButtonPayment.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.ButtonPayment.ForeColor = System.Drawing.Color.White;
            this.ButtonPayment.Location = new System.Drawing.Point(3, 198);
            this.ButtonPayment.Name = "ButtonPayment";
            this.ButtonPayment.RoundCorners = Coffee.Display.DisplayButton.Corners.All;
            this.ButtonPayment.Size = new System.Drawing.Size(180, 32);
            this.ButtonPayment.TabIndex = 2;
            this.ButtonPayment.TextButton = "Оплатить";
            this.ButtonPayment.Click += new System.EventHandler(this.ButtonPayment_Click);
            // 
            // panelParam1
            // 
            this.panelParam1.Controls.Add(this.ButtonArrowPrevParam1);
            this.panelParam1.Controls.Add(this.ButtonArrowBackParam1);
            this.panelParam1.Controls.Add(this.labelParam1);
            this.panelParam1.Location = new System.Drawing.Point(0, 80);
            this.panelParam1.Margin = new System.Windows.Forms.Padding(0, 2, 0, 2);
            this.panelParam1.Name = "panelParam1";
            this.panelParam1.Size = new System.Drawing.Size(186, 35);
            this.panelParam1.TabIndex = 7;
            // 
            // ButtonArrowPrevParam1
            // 
            this.ButtonArrowPrevParam1.BackColor = System.Drawing.Color.Transparent;
            this.ButtonArrowPrevParam1.Background = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(187)))), ((int)(((byte)(231)))));
            this.ButtonArrowPrevParam1.Disable = false;
            this.ButtonArrowPrevParam1.Dock = System.Windows.Forms.DockStyle.Right;
            this.ButtonArrowPrevParam1.Flip = false;
            this.ButtonArrowPrevParam1.Foreground = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(104)))), ((int)(((byte)(120)))));
            this.ButtonArrowPrevParam1.Location = new System.Drawing.Point(172, 0);
            this.ButtonArrowPrevParam1.Margin = new System.Windows.Forms.Padding(1, 2, 1, 2);
            this.ButtonArrowPrevParam1.Name = "ButtonArrowPrevParam1";
            this.ButtonArrowPrevParam1.Size = new System.Drawing.Size(14, 35);
            this.ButtonArrowPrevParam1.TabIndex = 7;
            this.ButtonArrowPrevParam1.View = true;
            this.ButtonArrowPrevParam1.Click += new System.EventHandler(this.ButtonArrowPrevParam1_Click);
            // 
            // ButtonArrowBackParam1
            // 
            this.ButtonArrowBackParam1.BackColor = System.Drawing.Color.Transparent;
            this.ButtonArrowBackParam1.Background = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(187)))), ((int)(((byte)(231)))));
            this.ButtonArrowBackParam1.Disable = false;
            this.ButtonArrowBackParam1.Dock = System.Windows.Forms.DockStyle.Left;
            this.ButtonArrowBackParam1.Flip = true;
            this.ButtonArrowBackParam1.Foreground = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(104)))), ((int)(((byte)(120)))));
            this.ButtonArrowBackParam1.Location = new System.Drawing.Point(0, 0);
            this.ButtonArrowBackParam1.Margin = new System.Windows.Forms.Padding(1, 2, 1, 2);
            this.ButtonArrowBackParam1.Name = "ButtonArrowBackParam1";
            this.ButtonArrowBackParam1.Size = new System.Drawing.Size(14, 35);
            this.ButtonArrowBackParam1.TabIndex = 6;
            this.ButtonArrowBackParam1.View = true;
            this.ButtonArrowBackParam1.Click += new System.EventHandler(this.ButtonArrowBackParam1_Click);
            // 
            // labelParam1
            // 
            this.labelParam1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelParam1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelParam1.ForeColor = System.Drawing.Color.White;
            this.labelParam1.Location = new System.Drawing.Point(10, 0);
            this.labelParam1.Name = "labelParam1";
            this.labelParam1.Size = new System.Drawing.Size(165, 35);
            this.labelParam1.TabIndex = 5;
            this.labelParam1.Text = "Напиток";
            this.labelParam1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelParam1.Click += new System.EventHandler(this.labelParam1_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.labelPrice);
            this.panel1.Controls.Add(this.displayDrinkCup1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 237);
            this.panel1.Name = "panel1";
            this.tableLayoutPanel1.SetRowSpan(this.panel1, 2);
            this.panel1.Size = new System.Drawing.Size(180, 72);
            this.panel1.TabIndex = 10;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::Coffee.Properties.Resources.coin;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.Location = new System.Drawing.Point(7, 18);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(40, 40);
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            // 
            // labelPrice
            // 
            this.labelPrice.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelPrice.ForeColor = System.Drawing.Color.White;
            this.labelPrice.Location = new System.Drawing.Point(53, 7);
            this.labelPrice.Name = "labelPrice";
            this.labelPrice.Size = new System.Drawing.Size(68, 62);
            this.labelPrice.TabIndex = 12;
            this.labelPrice.Text = "1000";
            this.labelPrice.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // displayDrinkCup1
            // 
            this.displayDrinkCup1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.displayDrinkCup1.Location = new System.Drawing.Point(125, 7);
            this.displayDrinkCup1.Margin = new System.Windows.Forms.Padding(0, 0, 0, 13);
            this.displayDrinkCup1.Name = "displayDrinkCup1";
            this.displayDrinkCup1.Size = new System.Drawing.Size(47, 62);
            this.displayDrinkCup1.TabIndex = 11;
            // 
            // DisplayDrinkParameters
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(187)))), ((int)(((byte)(231)))));
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "DisplayDrinkParameters";
            this.Size = new System.Drawing.Size(186, 312);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panelParam3.ResumeLayout(false);
            this.panelParam2.ResumeLayout(false);
            this.panelParam1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Coffee.Display.DisplayButton ButtonBack;
        private System.Windows.Forms.Label labelName;
        private Coffee.Display.DisplayButton ButtonPayment;
        private System.Windows.Forms.Panel panelParam1;
        private System.Windows.Forms.Label labelParam1;
        private Display.DisplayButtonArrow ButtonArrowBackParam1;
        private Display.DisplayButtonArrow ButtonArrowPrevParam1;
        private System.Windows.Forms.Panel panelParam3;
        private Display.DisplayButtonArrow ButtonArrowPrevParam3;
        private Display.DisplayButtonArrow ButtonArrowBackParam3;
        private System.Windows.Forms.Label labelParam3;
        private System.Windows.Forms.Panel panelParam2;
        private Display.DisplayButtonArrow ButtonArrowPrevParam2;
        private Display.DisplayButtonArrow ButtonArrowBackParam2;
        private System.Windows.Forms.Label labelParam2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label labelPrice;
        private Display.DisplayDrinkCup displayDrinkCup1;
    }
}
