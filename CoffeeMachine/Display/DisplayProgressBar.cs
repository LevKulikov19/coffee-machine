﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Coffee
{
    public partial class DisplayProgressBar : UserControl
    {
        private uint value = 0;
        public uint Value { 
            get => value;
            set {
                this.value = value;
                Refresh();
            } 
        }
        public Color Background { get; set; }
        public Color Foreground { get; set; }

        public DisplayProgressBar()
        {
            InitializeComponent();
            Background = ColorTranslator.FromHtml("#744F3D");
            Foreground = ColorTranslator.FromHtml("#712822");
            Refresh();
        }

        private void DisplayProgressBar_Paint(object sender, PaintEventArgs e)
        {
            RefreshProgressBar();
        }

        void RefreshProgressBar()
        {
            Graphics graphicsPaintSpace = this.CreateGraphics();
            BufferedGraphicsContext bufferedGraphicsContext = BufferedGraphicsManager.Current;
            BufferedGraphics bufferedGraphics = bufferedGraphicsContext.Allocate(graphicsPaintSpace, this.DisplayRectangle);
            Graphics gPaint = bufferedGraphics.Graphics;

            gPaint.Clear(Background);
            gPaint.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            Brush brush = new SolidBrush(Foreground);
            Rectangle rectangleProgress = new Rectangle(0,0, (int)(this.ClientRectangle.Width * Value / 100), this.ClientRectangle.Height);
            gPaint.FillRectangle(brush, rectangleProgress);

            bufferedGraphics.Render(graphicsPaintSpace);
        }

        private void DisplayProgressBar_Load(object sender, EventArgs e)
        {

        }
    }
}
