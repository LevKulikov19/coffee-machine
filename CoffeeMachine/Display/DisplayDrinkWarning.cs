﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Coffee.Display
{
    public partial class DisplayDrinkWarning : UserControl
    {
        public delegate void DisplayDrinkWarningEvent(object sender, Drink drink);
        public event DisplayDrinkWarningEvent Continue;

        private Drink drink;

        public DisplayDrinkWarning(Drink drink)
        {
            InitializeComponent();
            this.drink = drink;
        }

        private void ButtonContinue_Click(object sender, EventArgs e)
        {
            if (Continue != null) Continue(this, drink);

        }

    }
}
