﻿
namespace Coffee
{
    partial class DisplayPayment
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.labelParam3 = new System.Windows.Forms.Label();
            this.labelParam2 = new System.Windows.Forms.Label();
            this.labelParam1 = new System.Windows.Forms.Label();
            this.labelName = new System.Windows.Forms.Label();
            this.ButtonBack = new Coffee.Display.DisplayButton();
            this.ButtonCooking = new Coffee.Display.DisplayButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.labelPrice = new System.Windows.Forms.Label();
            this.labelCoins = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.labelParam3, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.labelParam2, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.labelParam1, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.labelName, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.ButtonBack, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.ButtonCooking, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 5);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 8;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 12F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(130, 187);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // labelParam3
            // 
            this.labelParam3.AutoSize = true;
            this.labelParam3.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelParam3.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelParam3.ForeColor = System.Drawing.Color.White;
            this.labelParam3.Location = new System.Drawing.Point(2, 92);
            this.labelParam3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelParam3.Name = "labelParam3";
            this.labelParam3.Size = new System.Drawing.Size(128, 17);
            this.labelParam3.TabIndex = 6;
            this.labelParam3.Text = "Напиток";
            this.labelParam3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelParam2
            // 
            this.labelParam2.AutoSize = true;
            this.labelParam2.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelParam2.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelParam2.ForeColor = System.Drawing.Color.White;
            this.labelParam2.Location = new System.Drawing.Point(2, 69);
            this.labelParam2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelParam2.Name = "labelParam2";
            this.labelParam2.Size = new System.Drawing.Size(128, 17);
            this.labelParam2.TabIndex = 5;
            this.labelParam2.Text = "Напиток";
            this.labelParam2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelParam1
            // 
            this.labelParam1.AutoSize = true;
            this.labelParam1.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelParam1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelParam1.ForeColor = System.Drawing.Color.White;
            this.labelParam1.Location = new System.Drawing.Point(2, 46);
            this.labelParam1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelParam1.Name = "labelParam1";
            this.labelParam1.Size = new System.Drawing.Size(128, 17);
            this.labelParam1.TabIndex = 4;
            this.labelParam1.Text = "Напиток";
            this.labelParam1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelName.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point);
            this.labelName.ForeColor = System.Drawing.Color.White;
            this.labelName.Location = new System.Drawing.Point(2, 23);
            this.labelName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(128, 17);
            this.labelName.TabIndex = 1;
            this.labelName.Text = "Напиток";
            this.labelName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // ButtonBack
            //  
            this.ButtonBack.Background = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(187)))), ((int)(((byte)(231)))));
            this.ButtonBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(113)))), ((int)(((byte)(40)))), ((int)(((byte)(34)))));
            this.ButtonBack.CornerRadius = 50;
            this.ButtonBack.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.ButtonBack.ForeColor = System.Drawing.Color.White;
            this.ButtonBack.Location = new System.Drawing.Point(2, 2);
            this.ButtonBack.Margin = new System.Windows.Forms.Padding(2);
            this.ButtonBack.Name = "ButtonBack";
            this.ButtonBack.RoundCorners = ((Coffee.Display.DisplayButton.Corners)((((Coffee.Display.DisplayButton.Corners.TopLeft | Coffee.Display.DisplayButton.Corners.TopRight) 
            | Coffee.Display.DisplayButton.Corners.BottomLeft) 
            | Coffee.Display.DisplayButton.Corners.BottomRight)));
            this.ButtonBack.Size = new System.Drawing.Size(45, 16);
            this.ButtonBack.TabIndex = 0;
            this.ButtonBack.TextButton = "Назад";
            this.ButtonBack.Click += new System.EventHandler(this.ButtonBack_Click);
            // 
            // ButtonCooking
            //  
            this.ButtonCooking.Background = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(187)))), ((int)(((byte)(231)))));
            this.ButtonCooking.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(113)))), ((int)(((byte)(40)))), ((int)(((byte)(34)))));
            this.ButtonCooking.CornerRadius = 50;
            this.ButtonCooking.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.ButtonCooking.ForeColor = System.Drawing.Color.White;
            this.ButtonCooking.Location = new System.Drawing.Point(2, 163);
            this.ButtonCooking.Margin = new System.Windows.Forms.Padding(2);
            this.ButtonCooking.Name = "ButtonCooking";
            this.ButtonCooking.RoundCorners = ((Coffee.Display.DisplayButton.Corners)((((Coffee.Display.DisplayButton.Corners.TopLeft | Coffee.Display.DisplayButton.Corners.TopRight) 
            | Coffee.Display.DisplayButton.Corners.BottomLeft) 
            | Coffee.Display.DisplayButton.Corners.BottomRight)));
            this.ButtonCooking.Size = new System.Drawing.Size(128, 22);
            this.ButtonCooking.TabIndex = 2;
            this.ButtonCooking.TextButton = "Приготовить";
            this.ButtonCooking.Click += new System.EventHandler(this.ButtonCooking_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.labelPrice);
            this.panel1.Controls.Add(this.labelCoins);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(2, 117);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.tableLayoutPanel1.SetRowSpan(this.panel1, 2);
            this.panel1.Size = new System.Drawing.Size(128, 42);
            this.panel1.TabIndex = 7;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.Location = new System.Drawing.Point(54, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(18, 32);
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // labelPrice
            // 
            this.labelPrice.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelPrice.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelPrice.ForeColor = System.Drawing.Color.White;
            this.labelPrice.Location = new System.Drawing.Point(-11, 9);
            this.labelPrice.Margin = new System.Windows.Forms.Padding(0);
            this.labelPrice.Name = "labelPrice";
            this.labelPrice.Size = new System.Drawing.Size(75, 19);
            this.labelPrice.TabIndex = 2;
            this.labelPrice.Text = "100";
            this.labelPrice.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelCoins
            // 
            this.labelCoins.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelCoins.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelCoins.ForeColor = System.Drawing.Color.White;
            this.labelCoins.Location = new System.Drawing.Point(64, 9);
            this.labelCoins.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelCoins.Name = "labelCoins";
            this.labelCoins.Size = new System.Drawing.Size(77, 19);
            this.labelCoins.TabIndex = 0;
            this.labelCoins.Text = "100";
            this.labelCoins.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // DisplayPayment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(187)))), ((int)(((byte)(231)))));
            this.Controls.Add(this.tableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "DisplayPayment";
            this.Size = new System.Drawing.Size(130, 187);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label labelParam3;
        private System.Windows.Forms.Label labelParam2;
        private System.Windows.Forms.Label labelParam1;
        private System.Windows.Forms.Label labelName;
        private Coffee.Display.DisplayButton ButtonBack;
        private Coffee.Display.DisplayButton ButtonCooking;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelCoins;
        private System.Windows.Forms.Label labelPrice;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}
