﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Coffee.Display
{
    public partial class DisplayButtonArrow : UserControl
    {
        public Color Background
        {
            get;
            set;
        }

        public Color Foreground
        {
            get;
            set;
        }

        public bool Flip
        {
            get;
            set;
        }

        private bool view;
        public bool View
        {
            get => view;
            set
            {
                view = value;
                Refresh();
            }
        }

        private bool disable;
        public bool Disable
        {
            get => disable;
            set
            {
                disable = value;
                Refresh();
            }
        }

        public DisplayButtonArrow()
        {
            InitializeComponent();
            view = true;
            Disable = false;
            Background = ColorTranslator.FromHtml("#10BBE7");
            Foreground = ColorTranslator.FromHtml("#296878");
            Refresh();
        }

        private void DisplayButtonArrow_Paint(object sender, PaintEventArgs e)
        {
            RefreshButtonArrow();
        }

        private void RefreshButtonArrow()
        {
            Graphics graphicsPaintSpace = this.CreateGraphics();
            BufferedGraphicsContext bufferedGraphicsContext = BufferedGraphicsManager.Current;
            BufferedGraphics bufferedGraphics = bufferedGraphicsContext.Allocate(graphicsPaintSpace, this.DisplayRectangle);
            Graphics gPaint = bufferedGraphics.Graphics;

            gPaint.Clear(Background);
            if (View)
            {
                gPaint.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                Color colorButton = Foreground;
                if (Disable) colorButton = DarkenColor(colorButton, 40);
                Brush brush = new SolidBrush(colorButton);
                Point[] points = new Point[3];
                if (Flip)
                {
                    points[0] = new Point(this.ClientRectangle.Width, 0);
                    points[1] = new Point(this.ClientRectangle.Width, this.ClientRectangle.Height);
                    points[2] = new Point(0, this.ClientRectangle.Height / 2);
                }
                else
                {
                    points[0] = new Point(0, 0);
                    points[1] = new Point(0, this.ClientRectangle.Height);
                    points[2] = new Point(this.ClientRectangle.Width, this.ClientRectangle.Height / 2);
                }
                gPaint.FillPolygon(brush, points);
            }

            bufferedGraphics.Render(graphicsPaintSpace);
        }

        private Color DarkenColor(Color colorIn, int percent)
        {
            if (percent < 0 || percent > 100)
                throw new ArgumentOutOfRangeException("percent");

            int a, r, g, b;

            a = colorIn.A;
            r = colorIn.R - (int)((colorIn.R / 100f) * percent);
            g = colorIn.G - (int)((colorIn.G / 100f) * percent);
            b = colorIn.B - (int)((colorIn.B / 100f) * percent);

            return Color.FromArgb(a/4, r, g, b);
        }
    }
}
