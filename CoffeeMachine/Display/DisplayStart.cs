﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Coffee
{
    public partial class DisplayStart : UserControl
    {
        public delegate void DisplayStartEvent();
        public event DisplayStartEvent Start;
        public DisplayStart()
        {
            InitializeComponent();
        }

        private void displayButtonMain1_Click(object sender, EventArgs e)
        {
            Start();
        }
    }
}
