﻿
namespace Coffee.Display
{
    partial class DisplayButton
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // DisplayButton
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DisplayButton";
            this.Size = new System.Drawing.Size(207, 96);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.DisplayButton_Paint);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.DisplayButton_MouseDown);
            this.MouseLeave += new System.EventHandler(this.DisplayButton_MouseLeave);
            this.MouseHover += new System.EventHandler(this.DisplayButton_MouseHover);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.DisplayButton_MouseUp);
            this.ResumeLayout(false);

        }

        #endregion
    }
}
