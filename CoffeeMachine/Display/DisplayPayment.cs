﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Coffee
{
    public partial class DisplayPayment : UserControl
    {
        public delegate void DisplayPaymentEvent(object sender, Drink drink);
        public event DisplayPaymentEvent Back;
        public event DisplayPaymentEvent Cooking;

        uint balance = 0;
        public uint Balance
        {
            get => balance;
            set
            {
                balance = value;
                RefreshBalance();
            }
        }

        public Drink Drink {
            get;
        }

        public DisplayPayment(Drink drink = null)
        {
            InitializeComponent();
            pictureBox1.BackgroundImage = global::Coffee.Properties.Resources.coin;
            ButtonCooking.Enabled = false;
            Drink = drink;
            InitializeLableDrink(drink);
        }

        private void InitializeLableDrink(Drink drink)
        {
            if (drink == null)
            {
                labelParam1.Text = string.Empty;
                labelParam2.Text = string.Empty;
                labelParam3.Text = string.Empty;
                labelName.Text = string.Empty;
                return;
            }

            labelName.Text = drink.Name;
            List<string> paramValue = drink.getNameCurrentValuesParametrs();
            if (paramValue.Count >= 1) labelParam1.Text = paramValue[0];
            if (paramValue.Count >= 2) labelParam2.Text = paramValue[1];
            if (paramValue.Count >= 3) labelParam3.Text = paramValue[2];
            labelPrice.Text = drink.Price.ToString();
        }

        private void ButtonBack_Click(object sender, EventArgs e)
        {
            Back(this, Drink);
        }

        private void ButtonCooking_Click(object sender, EventArgs e)
        {
            Cooking(this, Drink);
        }

        private void RefreshBalance()
        {
            labelCoins.Text = balance.ToString();
            if (Drink.Price <= balance) ButtonCooking.Enabled = true;
        }
    }
}
