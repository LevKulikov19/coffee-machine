﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;
using System.Windows.Forms;

namespace Coffee.Display
{
    public partial class DisplayButton : UserControl
    {
        public enum Corners
        {
            None = 0,
            TopLeft = 1,
            TopRight = 2,
            BottomLeft = 4,
            BottomRight = 8,
            All = TopLeft | TopRight | BottomLeft | BottomRight
        }

        public enum CustomButtonState
        {
            Normal = 1,
            Hot,
            Pressed,
        }

        public string TextButton
        {
            get;
            set;
        }

        public Color Background
        {
            get;
            set;
        }

        public int CornerRadius
        {
            get => m_CornerRadius;
            set
            {
                m_CornerRadius = value;
            }
        }


        public Corners RoundCorners
        {
            get => m_RoundCorners;
            set
            {
                m_RoundCorners = value;
            }
        }

        public DisplayButton()
        {
            InitializeComponent();
            RefreshButton();
        }

        private int m_CornerRadius = 50;
        private Corners m_RoundCorners;
        private CustomButtonState m_ButtonState = CustomButtonState.Normal;
        private bool keyPressed;
        private Rectangle contentRect;

        private void RefreshButton()
        {
            if (this.IsDisposed == true) return;
            Graphics graphicsPaintSpace = this.CreateGraphics();
            BufferedGraphicsContext bufferedGraphicsContext = BufferedGraphicsManager.Current;
            BufferedGraphics bufferedGraphics = bufferedGraphicsContext.Allocate(graphicsPaintSpace, this.DisplayRectangle);
            Graphics gPaint = bufferedGraphics.Graphics;

            gPaint.SmoothingMode = SmoothingMode.AntiAlias;
            gPaint.Clear(Background);

            Color fillColor;
            Color darkDarkColor = DarkenColor(this.BackColor, 10);
            Color lightColor = LightenColor(this.BackColor, 10);
            if (this.m_ButtonState == CustomButtonState.Hot)
            {
                fillColor = lightColor;
            }
            else if (this.m_ButtonState == CustomButtonState.Pressed)
            {
                fillColor = darkDarkColor;
            }
            else
            {
                fillColor = this.BackColor;
            }

            Rectangle r = this.ClientRectangle;
            GraphicsPath path = RoundRectangle(r, this.m_CornerRadius, this.m_RoundCorners);
            SolidBrush paintBrush = new SolidBrush(fillColor);

            Blend b = new Blend();
            b.Positions = new float[] { 0, 0.45F, 0.55F, 1 };
            b.Factors = new float[] { 0, 0, 1, 1 };

            gPaint.FillPath(paintBrush, path);
            paintBrush.Dispose();

            Pen drawingPen = new Pen(darkDarkColor);
            drawingPen.Dispose();

            bool inBounds = false;
            while (!inBounds && r.Width >= 1 && r.Height >= 1)
            {
                inBounds = path.IsVisible(r.Left, r.Top) &&
                            path.IsVisible(r.Right, r.Top) &&
                            path.IsVisible(r.Left, r.Bottom) &&
                            path.IsVisible(r.Right, r.Bottom);
                r.Inflate(-1, -1);

            }

            contentRect = r;
            DrawText(gPaint);
            bufferedGraphics.Render(graphicsPaintSpace);
        }

        private void DisplayButton_Paint(object sender, PaintEventArgs e)
        {
            RefreshButton();
        }

        private void DrawText(Graphics g)
        {
            SolidBrush TextBrush = new SolidBrush(this.ForeColor);

            RectangleF R = contentRect;

            if (!this.Enabled) TextBrush.Color = SystemColors.GrayText;

            StringFormat sf = new StringFormat(StringFormatFlags.NoWrap | StringFormatFlags.NoClip);

            if (ShowKeyboardCues)
                sf.HotkeyPrefix = System.Drawing.Text.HotkeyPrefix.Show;
            else
                sf.HotkeyPrefix = System.Drawing.Text.HotkeyPrefix.Hide;

            sf.Alignment = StringAlignment.Center;
            sf.LineAlignment = StringAlignment.Center;

            if (this.m_ButtonState == CustomButtonState.Pressed)
                R.Offset(1, 1);
            g.DrawString(TextButton, Font, TextBrush, R, sf);

        }

        public static GraphicsPath RoundedRect(Rectangle bounds, int radius)
        {
            int diameter = radius * 2;
            Size size = new Size(diameter, diameter);
            Rectangle arc = new Rectangle(bounds.Location, size);
            GraphicsPath path = new GraphicsPath();

            if (radius == 0)
            {
                path.AddRectangle(bounds);
                return path;
            }

            // верхняя левая дуга 
            path.AddArc(arc, 180, 90);

            // верхняя правая дуга 
            arc.X = bounds.Right - diameter;
            path.AddArc(arc, 270, 90);

            // нижняя правая дуга 
            arc.Y = bounds.Bottom - diameter;
            path.AddArc(arc, 0, 90);

            // нижняя левая дуга 
            arc.X = bounds.Left;
            path.AddArc(arc, 90, 90);

            path.CloseFigure();
            return path;
        }

        private GraphicsPath RoundRectangle(Rectangle r, int radius, Corners corners)
        {
            r.Width -= 1;
            r.Height -= 1;

            if (radius > (r.Width))
                radius = r.Width;
            if (radius > (r.Height))
                radius = r.Height;

            GraphicsPath path = new GraphicsPath();

            if (radius <= 0)
                path.AddRectangle(r);
            else
                if ((corners & Corners.TopLeft) == Corners.TopLeft)
                path.AddArc(r.Left, r.Top, radius, radius, 180, 90);
            else
                path.AddLine(r.Left, r.Top, r.Left, r.Top);

            if ((corners & Corners.TopRight) == Corners.TopRight)
                path.AddArc(r.Right - radius, r.Top, radius, radius, 270, 90);
            else
                path.AddLine(r.Right, r.Top, r.Right, r.Top);

            if ((corners & Corners.BottomRight) == Corners.BottomRight)
                path.AddArc(r.Right - radius, r.Bottom - radius, radius, radius, 0, 90);
            else
                path.AddLine(r.Right, r.Bottom, r.Right, r.Bottom);

            if ((corners & Corners.BottomLeft) == Corners.BottomLeft)
                path.AddArc(r.Left, r.Bottom - radius, radius, radius, 90, 90);
            else
                path.AddLine(r.Left, r.Bottom, r.Left, r.Bottom);

            path.CloseFigure();

            return path;
        }

        private Color DarkenColor(Color colorIn, uint percent)
        {
            if (percent > 100) percent = 100;

            int a, r, g, b;
            a = colorIn.A;
            r = colorIn.R - (int)((colorIn.R / 100f) * percent);
            g = colorIn.G - (int)((colorIn.G / 100f) * percent);
            b = colorIn.B - (int)((colorIn.B / 100f) * percent);

            return Color.FromArgb(a, r, g, b);
        }

        private Color LightenColor(Color colorIn, uint percent)
        {
            if (percent > 100) percent = 100;

            int a, r, g, b;
            a = colorIn.A;
            r = colorIn.R + (int)(((255f - colorIn.R) / 100f) * percent);
            g = colorIn.G + (int)(((255f - colorIn.G) / 100f) * percent);
            b = colorIn.B + (int)(((255f - colorIn.B) / 100f) * percent);

            return Color.FromArgb(a, r, g, b);
        }

        private void DisplayButton_MouseHover(object sender, EventArgs e)
        {
            m_ButtonState = CustomButtonState.Hot;
            keyPressed = true;
            RefreshButton();
        }

        private void DisplayButton_MouseLeave(object sender, EventArgs e)
        {
            m_ButtonState = CustomButtonState.Normal;
            keyPressed = false;
            RefreshButton();
        }

        private void DisplayButton_MouseUp(object sender, MouseEventArgs e)
        {
            if (keyPressed) m_ButtonState = CustomButtonState.Hot;
            else m_ButtonState = CustomButtonState.Normal;
            RefreshButton();
        }

        private void DisplayButton_MouseDown(object sender, MouseEventArgs e)
        {
            m_ButtonState = CustomButtonState.Pressed;
            RefreshButton();
        }
    }
}
