﻿using Coffee.Layers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Text;
using System.Windows.Forms;

namespace Coffee
{
    public partial class CookingArea : UserControl
    {
        public delegate void PreparationAreaAnimationEvent(IDrinkCooking drink);
        public event PreparationAreaAnimationEvent AnimationMoveMiddle;
        public event PreparationAreaAnimationEvent AnimationMoveEnd;

        IDrinkCooking drink;
        uint positionPercentage = 0;
        uint PositionPercentage
        {
            get => positionPercentage;
            set
            {
                if (value > 100) positionPercentage = 100;
                else positionPercentage = value;
            }
        }
        uint positionPercentageStop = 0;
        uint PositionPercentageStop
        {
            get => positionPercentageStop;
            set
            {
                if (value > 100) positionPercentageStop = 100;
                else positionPercentageStop = value;
            }
        }

        uint positionIcePercentage = 0;
        uint PositionIcePercentage
        {
            get => positionIcePercentage;
            set
            {
                if (value > 100) positionIcePercentage = 100;
                else positionIcePercentage = value;
            }
        }


        Bitmap imageIce;
        Bitmap imageCup;

        public CookingArea()
        {
            InitializeComponent();
            drink = null;
            BackColor = ColorTranslator.FromHtml("#4C2108");
            imageIce = global::Coffee.Properties.Resources.ice;
            imageCup = global::Coffee.Properties.Resources.СupWithoutTop;
            Refresh();
        }

        private void RefreshCookingArea()
        {
            Graphics graphicsPaintSpace = this.CreateGraphics();
            BufferedGraphicsContext bufferedGraphicsContext = BufferedGraphicsManager.Current;
            BufferedGraphics bufferedGraphics = bufferedGraphicsContext.Allocate(graphicsPaintSpace, this.DisplayRectangle);
            Graphics gPaint = bufferedGraphics.Graphics;

            gPaint.Clear(BackColor);
            gPaint.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            //жидкость
            if (fluidDraw != null)
            {
                Brush brushFluid = new SolidBrush(fluidDraw.MainColor);
                int widthFluid = 10;
                int heightFluid = ClientRectangle.Width / 2;
                int xFluid = ClientRectangle.Width / 2 - widthFluid / 2;
                int yFluid = 0;
                Rectangle rectangleFluid = new Rectangle(xFluid, yFluid, widthFluid, heightFluid);
                gPaint.FillRectangle(brushFluid, rectangleFluid);
            }

            //лед
            if (IceDraw != null)
            {
                int widthIce = 10;
                int heightICe = widthIce;
                int xIce = ClientRectangle.Width / 2 - widthIce / 2;
                int yIce = ClientRectangle.Width / 2 * (int)PositionIcePercentage / 100;
                Rectangle rectangleViewIce = new Rectangle(xIce, yIce, widthIce, heightICe);
                ImageAttributes wrapMode = new ImageAttributes();
                gPaint.DrawImage(imageIce, rectangleViewIce, 0, 0, imageIce.Width, imageIce.Height, GraphicsUnit.Pixel, wrapMode);
            }

            //раздатчик
            Brush brushDispennser = new SolidBrush(ColorTranslator.FromHtml("#FFCB7B"));
            int widthDispennser = 16;
            int heightDispennser = 8;
            int xDispennser = ClientRectangle.Width / 2 - widthDispennser / 2;
            int yDispennser = 0;
            int narrowing = 3;
            Point[] pointsDispennser = new Point[4];
            pointsDispennser[0] = new Point(xDispennser, yDispennser);
            pointsDispennser[1] = new Point(xDispennser + narrowing, yDispennser + heightDispennser);
            pointsDispennser[2] = new Point(xDispennser + widthDispennser - narrowing, yDispennser + heightDispennser);
            pointsDispennser[3] = new Point(xDispennser + widthDispennser, yDispennser);
            gPaint.FillPolygon(brushDispennser, pointsDispennser);


            //стакачик
            if (PositionPercentage != 0 && PositionPercentage != 100 && drink != null)
            {
                float ratioCup = ((float)imageCup.Width) / imageCup.Height;
                int heightCup = ClientRectangle.Height / 2;
                int widthCup = (int)(heightCup * ratioCup);

                int xCup = (ClientRectangle.Width) - ((int)(((ClientRectangle.Width + widthCup) * positionPercentage / 100)));
                int yCup = ClientRectangle.Height - heightCup;
                Rectangle rectangleViewCup = new Rectangle(xCup, yCup, widthCup, heightCup);
                ImageAttributes wrapMode = new ImageAttributes();
                gPaint.DrawImage(imageCup, rectangleViewCup, 0, 0, imageCup.Width, imageCup.Height, GraphicsUnit.Pixel, wrapMode);
            }

            //стекло
            Brush brushGlass = new SolidBrush(Color.FromArgb(127,71,26,0));
            gPaint.FillRectangle(brushGlass, ClientRectangle);

            bufferedGraphics.Render(graphicsPaintSpace);
            gPaint.Dispose();
        }

        public void Cooking(IDrinkCooking drink)
        {
            this.drink = drink;
            PullOutCup();
        }

        public void PullOutCup()
        {
            PositionPercentage = 0;
            PositionPercentageStop = 50;
            timerAnimateMove.Enabled = true;
            AnimationMoveMiddle += CookingArea_AnimationLayer;
            currentLayer = 0;
        }

        int currentLayer = 0;
        Fluid fluidDraw = null;
        Ice IceDraw = null;
        int IceCurrentCount = 0;

        private void CookingArea_AnimationLayer(IDrinkCooking drink)
        {
            AnimationMoveMiddle -= CookingArea_AnimationLayer;
            List<DrinkLayer> dirnkLayers = drink.getDrinkLayers();
            timerIce.Enabled = false;
            if (currentLayer < dirnkLayers.Count)
            {
                DrinkLayer layer = dirnkLayers[currentLayer];
                if (layer is Fluid)
                {
                    Fluid fluid = layer as Fluid;
                    fluidDraw = fluid;
                    timerLayers.Interval = (int)fluidDraw.Duration * 1000;
                    timerLayers.Enabled = true;
                    PositionIcePercentage = 0;
                    IceCurrentCount = 0;
                    Refresh();
                }
                else if (layer is Ice)
                {
                    Ice ice = layer as Ice;
                    fluidDraw = null;
                    IceDraw = ice;
                    timerIce.Interval = (int)(ice.Duration / ice.Count);
                    timerIce.Enabled = true;
                    PositionIcePercentage = 0;
                    Refresh();
                }
            }
            else
            {
                fluidDraw = null;
                IceDraw = null;
                PutAwayCup();
            }

        }

        public void PutAwayCup()
        {
            if (PositionPercentage != 100)
            {
                PositionPercentageStop = 100;
                timerAnimateMove.Enabled = true;
            }
            else
            {
                PositionPercentage = 0;
                drink = null;
            }
        }

        private void timerAnimateMove_Tick(object sender, EventArgs e)
        {
            if (PositionPercentage < PositionPercentageStop) positionPercentage++;
            else if (PositionPercentage < PositionPercentageStop) positionPercentage--;
            else
            {

                if (PositionPercentage == 0 || PositionPercentage == 100)
                {
                    if (AnimationMoveEnd != null) AnimationMoveEnd(drink);
                    drink = null;
                }
                else
                {
                    if (AnimationMoveMiddle != null) AnimationMoveMiddle(drink);
                }
                timerAnimateMove.Enabled = false;
            }
            Refresh();
        }

        private void timerLayers_Tick(object sender, EventArgs e)
        {
            currentLayer++;
            timerLayers.Enabled = false;
            CookingArea_AnimationLayer(drink);
            Refresh();
        }

        private void timerIce_Tick(object sender, EventArgs e)
        {
            PositionIcePercentage++;
            PositionIcePercentage++;
            PositionIcePercentage++;
            Refresh();
            if (positionIcePercentage == 100)
            {
                IceCurrentCount++;
                if (IceCurrentCount == IceDraw.Count)  currentLayer++;
                CookingArea_AnimationLayer(drink);
            }
        }

        private void CookingArea_Paint(object sender, PaintEventArgs e)
        {
            RefreshCookingArea();
        }
    }
}
